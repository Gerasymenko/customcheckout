**Custom Checkout system**
---
**Setup**
1. cd wp-content/plugins/mcheckout/resource/frontend - npm i
2. cd wp-content/plugins/mcheckout - composer install
---

***Integratioins*** 
- liqpay
- telegram
- turbosms
- novapost
- ukrpost

---
**Components**
1. Cart [vue_cart]
2. Modal add to cart [vue_modal_cart]
3. Checkout [vue_checkout]
4. Success [vue_success]

**Change log**
1. v2.0 - new ui
2. v1.1 - refactoring, add ukr post, nova post token
3. v1.0 - first version