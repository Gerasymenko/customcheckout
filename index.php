<?php
/*
 * Plugin Name: M Checkout
 * Version: 2.2.4.19
 * Plugin URI: #
 * Description: Custom checkout for woocommerce base on vue.
 * Author: Gerasart
 * Author URI: https://t.me/gerasart
 */

use Nadar\PhpComposerReader\ComposerReader;
use Nadar\PhpComposerReader\AutoloadSection;
use HaydenPierce\ClassFinder\ClassFinder;

require_once __DIR__ . '/vendor/autoload.php';

const M_CHECKOUT_GIT_SLAG = 'mcheckout';
const M_CHECKOUT_GIT_BRANCH = 'main';
const M_CHECKOUT_GIT_TOKEN = 'glpat-NDgZzxJN_r5ttqpaX8Q6';

define('M_CHECKOUT_BASENAME', plugin_basename(__FILE__));
define('M_CHECKOUT_PATH', plugin_dir_path(__FILE__));
define('M_CHECKOUT_URL', plugin_dir_url(__FILE__));
define('M_CHECKOUT_PAGE', $_SERVER['QUERY_STRING']);

ClassFinder::setAppRoot(M_CHECKOUT_PATH);

$reader = new ComposerReader(M_CHECKOUT_PATH . 'composer.json');
$section = new AutoloadSection($reader, AutoloadSection::TYPE_PSR4);

foreach ($section as $autoload) {
    try {
        $namespace = substr((string)$autoload->namespace, 0, -1);
        $classes = ClassFinder::getClassesInNamespace($namespace);

        foreach ($classes as $class) {
            new $class();
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

require 'vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/Gerasymenko/mcheckout',
    __FILE__,
    M_CHECKOUT_GIT_SLAG
);

$myUpdateChecker->setBranch(M_CHECKOUT_GIT_BRANCH);
$myUpdateChecker->setAuthentication(M_CHECKOUT_GIT_TOKEN);
$myUpdateChecker->getVcsApi()->enableReleaseAssets();
