<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

namespace MCheckout\Traits;

trait AjaxTrait
{
    /**
     * @return void
     */
    public static function declaration_ajax(): void
    {
        $class_methods = get_class_methods(static::class);

        foreach ($class_methods as $name) {
            $ajax = strpos($name, 'ajax');
            $noPrivate = strpos($name, 'nopriv');
            $short = str_replace(array('ajax_', 'nopriv_'), '', $name);

            if ($ajax === 0) {
                add_action('wp_ajax_' . $short, array(static::class, $name));
            }

            if ($noPrivate === 5) {
                add_action('wp_ajax_nopriv_' . $short, array(static::class, $name));
            }
        }
    }

    /**
     * @param $var
     *
     * @return mixed|null
     */
    public static function queryPostVars($var)
    {
        return $_POST[$var] ?? null;
    }

    /**
     * @param $var
     *
     * @return mixed|null
     */
    public static function queryGetVars($var)
    {
        return $_GET[$var] ?? null;
    }
}
