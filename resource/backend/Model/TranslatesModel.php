<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Model;

use MCheckout\Admin\AdminTranslatesPostType;

class TranslatesModel
{
    /**
     * @param string $key
     * @param string $value
     *
     * @return void
     */
    public static function create(string $key, string $value)
    {
        $postId = wp_insert_post([
            'post_title' => $key,
            'post_status' => 'publish',
            'post_type' => 'm_translates',
        ]);

        self::setKey((int)$postId, $key);

        if ($value) {
            self::setTitle((int)$postId, $value);
        }
    }

    /**
     * @return array
     */
    public static function getList(): array
    {
        $args = array(
            'post_type' => AdminTranslatesPostType::POST_TYPE,
            'numberposts' => -1,
        );

        $posts = get_posts($args);
        $result = [];


        foreach ($posts as $post) {
            $key = self::getKey($post->ID);

            $result[$key] = self::getTitle($post->ID);
        }

        return $result;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public static function getKey(int $id)
    {
        return get_post_meta($id, 'mcheckout_box_key', true);
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public static function getTitle(int $id)
    {
        return get_post_meta($id, 'mcheckout_box_title', true);
    }

    /**
     * @param int $id
     * @param string $value
     *
     * @return void
     */
    public static function setKey(int $id, string $value)
    {
        update_post_meta($id, 'mcheckout_box_key', $value);
    }

    /**
     * @param int $id
     * @param string $value
     *
     * @return void
     */
    public static function setTitle(int $id, string $value)
    {
        update_post_meta($id, 'mcheckout_box_title', $value);
    }
}
