<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

class AdminTranslateFields
{
    private const META_BOX = 'add_meta_box_callback';

    private $config = '{"title":"Translates","prefix":"mcheckout_box_","domain":"mcheckout","class_name":"pt_post_subtitle","post-type":["m_translates"],"context":"advanced","priority":"high",
    "fields":[{"type":"textarea","label":"Title","id":"mcheckout_box_title"}, {"type":"text","label":"Key","id":"mcheckout_box_key"}]}';

    public function __construct()
    {
        $this->config = json_decode($this->config, true);
        add_action('add_meta_boxes', [$this, 'add_meta_boxes']);
        add_action('save_post', [$this, 'save_post']);
    }

    /**
     * @return void
     */
    public function add_meta_boxes(): void
    {
        foreach ($this->config['post-type'] as $screen) {
            add_meta_box(
                sanitize_title($this->config['title']),
                $this->config['title'],
                [$this, self::META_BOX],
                $screen,
                $this->config['context'],
                $this->config['priority']
            );
        }
    }

    /**
     * @param $post_id
     *
     * @return void
     */
    public function save_post($post_id): void
    {
        foreach ($this->config['fields'] as $field) {
            if (isset($_POST[$field['id']])) {
                $sanitized = $_POST[$field['id']];
                update_post_meta($post_id, $field['id'], $sanitized);
            }
        }
    }

    /**
     * @return void
     */
    public function add_meta_box_callback(): void
    {
        $this->fieldsTable();
    }

    /**
     * @return void
     */
    private function fieldsTable(): void
    {
        ?>
        <table class="form-table" role="presentation">
        <tbody><?php
        foreach ($this->config['fields'] as $field) {
            ?>
            <tr>
            <th scope="row"><?php $this->label($field); ?></th>
            <td><?php $this->field($field); ?></td>
            </tr><?php
        }
        ?></tbody>
        </table><?php
    }

    /**
     * @param $field
     *
     * @return void
     */
    private function label($field): void
    {
        printf(
            '<label class="" for="%s">%s</label>',
            $field['id'],
            $field['label']
        );
    }

    /**
     * @param $field
     *
     * @return void
     */
    private function field($field): void
    {
        switch ($field['type']) {
            case 'text':
                $this->input($field);
                break;
            case 'textarea':
                $this->text($field);
                break;
        }
    }

    /**
     * @param $field
     *
     * @return void
     */
    private function input($field): void
    {
        printf(
            '<input class="regular-text %s" id="%s" name="%s" %s type="%s" value="%s">',
            $field['class'] ?? '',
            $field['id'],
            $field['id'],
            isset($field['pattern']) ? "pattern='{$field['pattern']}'" : '',
            $field['type'],
            $this->value($field)
        );
    }

    /**
     * @param $field
     *
     * @return void
     */
    private function text($field): void
    {
        printf(
            '<textarea class="regular-text" id="%s" name="%s" >%s</textarea>',
            $field['id'],
            $field['id'],
            $this->value($field)
        );
    }

    /**
     * @param $field
     *
     * @return array|mixed|string|string[]
     */
    private function value($field)
    {
        global $post;

        if (metadata_exists('post', $post->ID, $field['id'])) {
            $value = get_post_meta($post->ID, $field['id'], true);
        } else if (isset($field['default'])) {
            $value = $field['default'];
        } else {
            return '';
        }

        return str_replace('\u0027', "'", $value);
    }
}
