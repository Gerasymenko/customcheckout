<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

use MCheckout\Service\Json;

class Settings
{
    public const LIQPAY_PUBLIC_FIELD = 'm_checkout_liqpay_public';
    public const LIQPAY_PRIVATE_FIELD = 'm_checkout_liqpay_private';
    public const LIQPAY_SANDBOX_FIELD = 'm_checkout_liqpay_sandbox';
    public const NOVAPOST_TOKEN_FIELD = 'm_checkout_novapost_token';

    public const SETTINGS_FIELD = [
        'SUCCESS_PAGE' => 'm_checkout_success_page',
        'CHECKOUT_PAGE' => 'm_checkout_page',
        'CACHE' => 'm_checkout_is_cache',
        'DEBUG' => 'm_checkout_is_debug',
        'LOGS' => 'm_checkout_is_logs',
        'LOGO_URL' => 'm_checkout_logo_url',
    ];

    public const SMS_FIELD = [
        'LOGIN' => 'm_checkout_sms_login',
        'PASSWORD' => 'm_checkout_sms_password',
        'SENDER' => 'm_checkout_sms_sender',
        'TEXT' => 'm_checkout_sms_sender_text',
    ];

    public const TELEGRAM_FIELD = [
        'TOKEN' => 'm_checkout_telegram_token',
        'IDS' => 'm_checkout_telegram_groups_ids',
        'IS_DEBUG' => 'm_checkout_telegram_is_debug',
    ];

    public const DEPOSIT_FIELD = [
        'ENABLE' => 'm_checkout_deposit_enable',
        'VALUE' => 'm_checkout_deposit_value',
        'TYPE' => 'm_checkout_deposit_type',
    ];

    public const PAY_PARTS_PRIVAT_FIELD = [
        'STORE_ID' => 'm_checkout_privat_store_id',
        'PASSWORD' => 'm_checkout_privat_password',
        'SEND_BOX_MODE' => 'm_checkout_privat_send_box_mode',
        'RECIPIENT_ID' => 'm_checkout_privat_recipient_id',
        'PREFIX' => 'm_checkout_privat_prefix',
        'MERCHANT_TYPE' => 'm_checkout_privat_merchant_type',
        'IS_DEBUG' => 'm_checkout_privat_is_debug',
    ];

    /**
     * @return bool
     */
    public static function isLiqpaySandbox(): bool
    {
        return get_option(self::LIQPAY_SANDBOX_FIELD) === 'true';
    }

    /**
     * @return bool
     */
    public static function isNeedCache(): bool
    {
        return get_option(self::SETTINGS_FIELD['CACHE']) === 'true';
    }

    /**
     * @return bool
     */
    public static function isNeedDebug(): bool
    {
        return get_option(self::SETTINGS_FIELD['DEBUG']) === 'true';
    }

    /**
     * @return bool
     */
    public static function isNeedLogs(): bool
    {
        return get_option(self::SETTINGS_FIELD['LOGS']) === 'true';
    }

    /**
     * @return bool
     */
    public static function isTelegramDebug(): bool
    {
        return get_option(self::TELEGRAM_FIELD['IS_DEBUG']) === 'true';
    }

    /**
     * @return bool
     */
    public static function isPayPartSandboxMode(): bool
    {
        return get_option(self::PAY_PARTS_PRIVAT_FIELD['IS_DEBUG']) === 'true';
    }

    /**
     * @return bool
     */
    public static function isDepositEnable(): bool
    {
        return get_option(self::DEPOSIT_FIELD['ENABLE']) === 'true';
    }

    /**
     * @return string
     */
    public static function getSuccessPage(): string
    {
        $defaultPage = '/success';
        $optionPage = get_option(self::SETTINGS_FIELD['SUCCESS_PAGE']);

        if ($optionPage) {
            $pageData = Json::decode($optionPage);

            //Support polylang
            if (function_exists('pll_get_post')) {
                $polylangPost = pll_get_post((int)$pageData['value']);

                return get_the_permalink($polylangPost);
            }

            return get_permalink((int)$pageData['value']) ?: $defaultPage;
        }

        return $defaultPage;
    }

    /**
     * @return string
     */
    public static function getCheckoutPage(): string
    {
        $defaultPage = 'checkout';
        $optionPage = get_option(self::SETTINGS_FIELD['CHECKOUT_PAGE']);

        if ($optionPage) {
            $pageData = Json::decode($optionPage);

            return get_post_field( 'post_name', (int)$pageData['value']) ?: $defaultPage;
        }

        return $defaultPage;
    }
}
