<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

use MCheckout\Service\Json;

class AdminLocalVars
{
    public static function getAdminVars(): array
    {
        return [
            'mCheckoutOptions' => [
                'liqpay' => [
                    'public' => get_option(Settings::LIQPAY_PUBLIC_FIELD, ''),
                    'private' => get_option(Settings::LIQPAY_PRIVATE_FIELD, ''),
                    'isSandbox' => Settings::isLiqpaySandbox(),
                ],
                'sms' => [
                    'login' => get_option(Settings::SMS_FIELD['LOGIN'], ''),
                    'password' => get_option(Settings::SMS_FIELD['PASSWORD'], ''),
                    'sender' => get_option(Settings::SMS_FIELD['SENDER'], ''),
                    'text' => get_option(Settings::SMS_FIELD['TEXT'], ''),
                ],
                'telegram' => [
                    'token' => get_option(Settings::TELEGRAM_FIELD['TOKEN'], ''),
                    'groups_ids' => get_option(Settings::TELEGRAM_FIELD['IDS'], ''),
                    'isDebug' => Settings::isTelegramDebug(),
                ],
                'novapost' => [
                    'token' => get_option(Settings::NOVAPOST_TOKEN_FIELD, '')
                ],
                'settings' => [
                    'isCache' => Settings::isNeedCache(),
                    'isDebug' => Settings::isNeedDebug(),
                    'isLogs' => Settings::isNeedLogs(),
                    'successPage' => get_option(Settings::SETTINGS_FIELD['SUCCESS_PAGE']) ? Json::decode(get_option(Settings::SETTINGS_FIELD['SUCCESS_PAGE'])) : '',
                    'checkoutPage' => get_option(Settings::SETTINGS_FIELD['CHECKOUT_PAGE']) ? Json::decode(get_option(Settings::SETTINGS_FIELD['CHECKOUT_PAGE'])) : '',
                ],
                'payParts' => [
                    'storeId' => get_option(Settings::PAY_PARTS_PRIVAT_FIELD['STORE_ID'], ''),
                    'password' => get_option(Settings::PAY_PARTS_PRIVAT_FIELD['PASSWORD'], ''),
                    'sendBoxMode' => get_option(Settings::PAY_PARTS_PRIVAT_FIELD['SEND_BOX_MODE'], 'hold'),
                    'recipientId' => get_option(Settings::PAY_PARTS_PRIVAT_FIELD['RECIPIENT_ID'], ''),
                    'prefix' => get_option(Settings::PAY_PARTS_PRIVAT_FIELD['PREFIX'], 'ORDER'),
                    'merchantType' => get_option(Settings::PAY_PARTS_PRIVAT_FIELD['MERCHANT_TYPE'], 'PP'),
                    'isDebug' => Settings::isPayPartSandboxMode(),
                ],
                'deposit' => [
                    'enable' => Settings::isDepositEnable(),
                    'type' => get_option(Settings::DEPOSIT_FIELD['TYPE'], 'percentage'),
                    'value' => get_option(Settings::DEPOSIT_FIELD['VALUE'], 50),
                ],
                'logoUrl' => get_option(Settings::SETTINGS_FIELD['LOGO_URL'], ''),
            ],
        ];
    }
}
