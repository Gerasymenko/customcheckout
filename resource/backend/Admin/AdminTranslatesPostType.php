<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

use MCheckout\Model\TranslatesModel;

class AdminTranslatesPostType
{
    public const POST_TYPE = 'm_translates';

    public function __construct()
    {
        add_action('init', [__CLASS__, 'registerTranslates']);
        add_filter('pll_get_post_types', [$this, 'add_cpt_to_pll'], 10, 2);
        add_filter('manage_m_translates_posts_columns', [$this, 'addColumn']);
        add_action('manage_m_translates_posts_custom_column', [$this, 'addColumnValues'], 10, 2);
        add_action('admin_enqueue_scripts', [$this, 'addScripts'], 10, 1);
        add_action('pre_get_posts', [$this, 'translateAdminSearch']);
    }

    /**
     * Snippet Name: Add admin script on custom post types
     */
    public function addScripts($hook)
    {
        global $post;
        if (isset($post->post_type) && ($hook === 'edit.php') && 'm_translates' === $post->post_type) {
            wp_enqueue_style('mcheckout-css', M_CHECKOUT_URL . '/resource/backend/Admin/Assets/mcheckout-admin.css', false, '1.1', 'all');
            wp_enqueue_script('mcheckout-admin', M_CHECKOUT_URL . '/resource/backend/Admin/Assets/mcheckout-admin.js');
        }
    }

    /**
     * @param $column
     * @param $postId
     *
     * @return void
     */
    public function addColumnValues($column, $postId): void
    {
        switch ($column) {
            case 'id':
                echo $postId . '<img class="mchecheckoutSave" src="' . M_CHECKOUT_URL . '/resource/backend/Admin/Assets/save.png' . '" />';
                break;
            case 'tit':
                echo '<textarea class="mch-field m_translate_title">' . TranslatesModel::getTitle($postId) . '</textarea>';
                break;
            case 'key':
                echo get_the_title($postId);
                break;
        }
    }

    /**
     * @param $columns
     *
     * @return mixed
     */
    public function addColumn($columns)
    {
        unset($columns['date']);
        unset($columns['title']);
        $columns['id'] = __('ID');
        $columns['key'] = __('Key');
        $columns['tit'] = __('Title');
        return $columns;
    }

    /**
     * @param $post_types
     *
     * @return mixed
     */
    public function add_cpt_to_pll($post_types)
    {
        $post_types['m_translates'] = 'm_translates';

        return $post_types;
    }

    /**
     * @return void
     */
    public static function registerTranslates(): void
    {
        register_post_type(self::POST_TYPE, [
            'label' => null,
            'labels' => [
                'name' => 'Translates',
                'singular_name' => __('Checkout Translates'),
                'menu_name' => __('Checkout Translates'),
            ],
            'public' => true,
            'has_archive' => false,
            'show_in_menu' => false,
            'menu_position' => 100,
            'menu_icon' => 'dashicons-welcome-add-page',
            'supports' => [''],
            'rewrite' => ['with_front' => false, 'slug' => false],
        ]);
    }


    public function translateAdminSearch($query)
    {
        $customFields = [
            "mcheckout_box_title",
            "mcheckout_box_key",
        ];

        if (!is_admin())
            return;

        if ($query->query['post_type'] != self::POST_TYPE)
            return;

        $searchTerm = $query->query_vars['s'];

        $query->query_vars['s'] = '';

        if ($searchTerm != '') {
            $metaQuery = ['relation' => 'OR'];

            foreach ($customFields as $field) {
                $metaQuery[] = [
                    'key' => $field,
                    'value' => $searchTerm,
                    'compare' => 'LIKE'
                ];
            }

            $query->set('meta_query', $metaQuery);
        };
    }
}
