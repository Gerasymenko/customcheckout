<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin\Ajax;

use MCheckout\Model\TranslatesModel;
use MCheckout\Traits\AjaxTrait;

class TranslatesAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    /**
     * [mcheckout_box_title, mcheckout_box_key]
     *
     * @return void
     */
    public static function ajax_saveTranslateMCheckout(): void
    {
        $data = self::queryPostVars('data');

        TranslatesModel::setTitle((int)$data['id'], $data['title']);

        wp_send_json_success($data);
    }
}
