<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin\Ajax;

use MCheckout\Admin\Settings;
use MCheckout\Traits\AjaxTrait;

class LogoAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    public static function ajax_logoUploadCallback()
    {
        $arr_img_ext = ['image/png', 'image/jpeg', 'image/jpg', 'image/gif', 'image/svg+xml'];
        $upload_overrides = ['test_form' => false];
        $uploadFile = [];
        $fileData = [];

        if (!function_exists('wp_handle_upload')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
        }

        if ($_FILES && count($_FILES) > 0) {
            foreach ($_FILES as $file) {
                $fileData = $file;

                if (in_array($file['type'], $arr_img_ext)) {
                    $uploadFile = wp_handle_upload($file, $upload_overrides);
                }
            }

            if (isset($uploadFile['url'])) {
                update_option(Settings::SETTINGS_FIELD['LOGO_URL'], $uploadFile['url']);

                wp_send_json_success(['data' => [
                    'url' => $uploadFile['url'],
                ]]);

                wp_die();
            } else {
                wp_send_json_error(['data' => $fileData]);
            }
        }
    }
}
