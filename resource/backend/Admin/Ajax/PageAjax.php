<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin\Ajax;

use MCheckout\Traits\AjaxTrait;

class PageAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    public static function ajax_getPagesMCheckout(): void
    {
        $pages = get_posts([
            'numberposts' => -1,
            'orderby' => 'modified',
            'post_type' => 'page',
            'order' => 'DESC'
        ]);

        wp_send_json_success($pages);
    }
}
