<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin\Ajax;

use MCheckout\Admin\Settings;
use MCheckout\Service\Json;
use MCheckout\Traits\AjaxTrait;

class SaveSettingsAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    /**
     * @return void
     */
    public static function ajax_saveSettingsMCheckout(): void
    {
        $data = self::queryPostVars('settings');

        if (isset($data)) {
            update_option(Settings::SETTINGS_FIELD['CACHE'], $data['isCache']);
            update_option(Settings::SETTINGS_FIELD['DEBUG'], $data['isDebug']);
            update_option(Settings::SETTINGS_FIELD['LOGS'], $data['isLogs']);

            if ($data['successPage']) {
                update_option(Settings::SETTINGS_FIELD["SUCCESS_PAGE"], Json::encode($data['successPage']));
            }

            if ($data['checkoutPage']) {
                update_option(Settings::SETTINGS_FIELD["CHECKOUT_PAGE"], Json::encode($data['checkoutPage']));
            }
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_saveLiqpaySettingsMCheckout(): void
    {
        $data = self::queryPostVars('liqpay');

        if (isset($data)) {
            update_option(Settings::LIQPAY_PUBLIC_FIELD, $data['public']);
            update_option(Settings::LIQPAY_PRIVATE_FIELD, $data['private']);
            update_option(Settings::LIQPAY_SANDBOX_FIELD, $data['isSandbox']);
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_saveSmsSettingsMCheckout(): void
    {
        $data = self::queryPostVars('sms');

        if (isset($data)) {
            update_option(Settings::SMS_FIELD['LOGIN'], $data['login']);
            update_option(Settings::SMS_FIELD['PASSWORD'], $data['password']);
            update_option(Settings::SMS_FIELD['SENDER'], stripslashes($data['sender']));
            update_option(Settings::SMS_FIELD['TEXT'], $data['text']);
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_saveTelegramSettingsMCheckout(): void
    {
        $data = self::queryPostVars('telegram');

        if (isset($data)) {
            update_option(Settings::TELEGRAM_FIELD['TOKEN'], $data['token']);
            update_option(Settings::TELEGRAM_FIELD['IDS'], $data['groups_ids']);
            update_option(Settings::TELEGRAM_FIELD['IS_DEBUG'], $data['isDebug']);
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_saveNovapostSettingsMCheckout(): void
    {
        $data = self::queryPostVars('novapost');

        if (isset($data)) {
            update_option(Settings::NOVAPOST_TOKEN_FIELD, $data['token']);
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_savePayPartSettingsMCheckout()
    {
        $data = self::queryPostVars('payParts');

        if (isset($data)) {
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['STORE_ID'], $data['storeId']);
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['PASSWORD'], $data['password']);
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['SEND_BOX_MODE'], $data['sendBoxMode']);
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['RECIPIENT_ID'], $data['recipientId']);
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['MERCHANT_TYPE'], $data['merchantType']);
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['PREFIX'], $data['prefix']);
            update_option(Settings::PAY_PARTS_PRIVAT_FIELD['IS_DEBUG'], $data['isDebug']);
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_saveDepositSettingsMCheckout()
    {
        $data = self::queryPostVars('deposit');

        if (isset($data)) {
            update_option(Settings::DEPOSIT_FIELD['ENABLE'], $data['enable']);
            update_option(Settings::DEPOSIT_FIELD['VALUE'], $data['value']);
            update_option(Settings::DEPOSIT_FIELD['TYPE'], $data['type']);
        }

        wp_send_json_success();
    }
}
