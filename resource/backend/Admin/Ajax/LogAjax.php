<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin\Ajax;

use MCheckout\Model\Resource\LogResourceModel;
use MCheckout\Traits\AjaxTrait;

class LogAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    /**
     * @action removeLogs
     * @return void
     */
    public static function ajax_removeLogsMCheckout(): void
    {
        LogResourceModel::delete();

        wp_send_json_success(['status' => 'ok']);
    }

    /**
     * @action getLogs
     * @return void
     */
    public static function ajax_getLogsMCheckout(): void
    {
        wp_send_json_success(LogResourceModel::getList());
    }
}
