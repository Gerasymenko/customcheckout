<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

use MCheckout\Service\AssetsLoader;

class AdminSetup
{
    private const TITLE = 'M Checkout';
    private const SLUG = 'm_checkout';

    public function __construct()
    {
        add_action('admin_menu', array($this, 'addAdminPage'), 99);
        add_filter('plugin_action_links_' . M_CHECKOUT_BASENAME, [$this, 'plugin_action_links']);
    }

    /**
     * @param $vars
     */
    public static function localeVars($vars): void
    {
        foreach ($vars as $key => $value) {
            echo "<script>";
            if (is_array($value) || is_object($value)) {
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            } elseif (is_string($value)) {
                $clear = preg_replace('/\R+/', " ", $value);
                $value = "'{$clear}'";
            }

            if ($value) {
                echo "window.{$key} = {$value};" . "\n";
            } else {
                echo "window.{$key} = '';" . "\n";
            }

            echo "</script>";
        }
    }

    /**
     * @param null $class
     *
     * @return string
     * @throws \ReflectionException
     */
    public static function getTrueClassName($class = null): string
    {
        return (new \ReflectionClass($class))->getShortName();
    }

    /**
     * @return void
     */
    public function addAdminPage(): void
    {
        add_menu_page(
            self::TITLE,
            self::TITLE,
            'edit_posts',
            self::SLUG,
            [$this, 'pageInner'],
            M_CHECKOUT_URL . '/resource/frontend/src/assets/images/logo.png'
        );

        add_submenu_page(self::SLUG,
            'Translates',
            'Translates',
            'manage_options',
            'edit.php?post_type='.AdminTranslatesPostType::POST_TYPE
        );
    }

    /**
     * @return void
     */
    public function pageInner(): void
    {
        AssetsLoader::assetsAdmin();
        echo "<div id='m-checkout-admin'></div>";
        self::localeVars(AdminLocalVars::getAdminVars());
    }

    /**
     * @param $links
     *
     * @return mixed
     */
    public function plugin_action_links($links)
    {
        $settings_link = '<a href="' . menu_page_url(self::SLUG, false) . '">' . esc_html(__('Settings', 'custom')) . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
