<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

class AdminPageTemplates
{
    const CHECKOUT_TEMPLATE = 'template-checkout.php';
    const SUCCESS_TEMPLATE = 'template-success.php';

    public function __construct()
    {
        add_filter('page_template', [$this, 'wpPageTemplates']);
        add_filter('theme_page_templates', [$this, 'wpTemplates'], 10, 4);
    }

    public function wpPageTemplates($page_template)
    {
        if (get_page_template_slug() == self::CHECKOUT_TEMPLATE) {
            $page_template = M_CHECKOUT_PATH . '/templates/'.self::CHECKOUT_TEMPLATE;
        }

        if (get_page_template_slug() == self::SUCCESS_TEMPLATE) {
            $page_template = M_CHECKOUT_PATH . '/templates/'.self::SUCCESS_TEMPLATE;
        }

        return $page_template;
    }

    /**
     * Add "Custom" template to page attirbute template section.
     */
    public function wpTemplates($post_templates, $wp_theme, $post, $post_type)
    {
        $post_templates[self::CHECKOUT_TEMPLATE] = __('MCheckout Checkout');
        $post_templates[self::SUCCESS_TEMPLATE] = __('MCheckout Success');

        return $post_templates;
    }
}
