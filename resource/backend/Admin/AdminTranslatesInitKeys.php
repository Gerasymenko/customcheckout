<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Admin;

use MCheckout\Model\TranslatesModel;
use MCheckout\Woocommerce\Payment\Cash\CashPaymentMethod;
use MCheckout\Woocommerce\Payment\CreditCard\CreditCardPaymentMethod;
use MCheckout\Woocommerce\Payment\Liqpay\LiqpayPaymentMethod;
use MCheckout\Woocommerce\Payment\LocalPayment\LocalPaymentMethod;
use MCheckout\Woocommerce\Payment\OtherPayment\OtherPayment;
use MCheckout\Woocommerce\Payment\PrivatPayParts\PayPartsPaymentMethod;
use MCheckout\Woocommerce\Shipping\LocalDelivery\LocalDeliveryMethod;
use MCheckout\Woocommerce\Shipping\Novapost\NovaPostMethod;
use MCheckout\Woocommerce\Shipping\OtherDelivery\OtherDeliveryMethod;
use MCheckout\Woocommerce\Shipping\ShopDelivery\ShopDeliveryMethod;
use MCheckout\Woocommerce\Shipping\Ukrpost\UkrPostMethod;

class AdminTranslatesInitKeys
{
    public function __construct()
    {
        add_action('init', [$this, 'createTranslates']);
    }

    /**
     * @return void
     */
    public function createTranslates()
    {
        if (class_exists('WC_Shipping_Method')) {
            $keys = $this->defaultTranslates();

            if (!function_exists('post_exists')) {
                require_once(ABSPATH . 'wp-admin/includes/post.php');
            }

            foreach ($keys as $key => $value) {
                if (!post_exists($key)) {
                    TranslatesModel::create($key, $value);
                }
            }
        }
    }

    public function defaultTranslates(): array
    {
        return [
            LocalDeliveryMethod::CODE . '_title' => 'Доставка і збірка',
            LocalDeliveryMethod::CODE . '_subtitle' => 'Безкоштовна доставка і збірка замовленя',
            NovaPostMethod::CODE . '_title' => 'Адресна доставка Новою Поштою',
            NovaPostMethod::CODE . '_subtitle' => 'Безкоштовна доставка за вашою адресою до під’їзду.',
            OtherDeliveryMethod::CODE . '_title' => 'Мені потрібен інший спосіб доставки',
            OtherDeliveryMethod::CODE . '_subtitle' => "Ми зв'яжемось з Вами, щоб обговорити деталі.",
            ShopDeliveryMethod::CODE . '_title' => 'Самовивіз зі складу',
            ShopDeliveryMethod::CODE . '_subtitle' => 'Склад знаходиться за адресою',
            UkrPostMethod::CODE . '_title' => 'Адресна доставка Укр Поштою',
            UkrPostMethod::CODE . '_subtitle' => '',
            'contact_info_title' => 'Контактна інформація',
            'contact_name_title' => 'Ваше повне ім’я',
            'contact_phone_title' => 'Телефон',
            'contact_email_title' => 'E-mail',
            'delivery_title' => 'Доставка',
            'delivery_city_title' => 'Місто',
            'delivery_address_title' => 'Адреса',
            'payment_title' => 'Форма оплати',
            'pre_sale_title' => 'Передоплата',
            'pre_sale_full' => 'Вся сума',
            'pre_sale_presale' => 'Половина суми',
            'pre_sale_description' => ' Якщо товар виробляється під замовлення, нам достатньо передоплати в розмірі 50%. Для оформлення замовлення на товар, що є в наявності, необхідно сплатити 100% від вартості замовлення. ',
            'cart_title' => 'Ваше замовлення',
            'cart_sum_title' => 'Загалом',
            'cart_presale_title' => 'Друга оплата:',
            'cart_final_title' => 'До оплати без вартості доставки',
            'cart_note_title' => 'Термін виконання: До 20 днів після передоплати',
            'cart_place_order_title' => 'Оформити замовлення',
            'cart_return_title' => 'Повернутися до покупок',
            'order_note_title' => 'Примітки до замовлень',
            'order_note_subtitle' => 'Нотатки до вашого замовлення',
            'order_note_placeholder' => 'Додаткова інформація чи побажання',
            LiqpayPaymentMethod::CODE . '_title' => 'Онлайн оплата через LiqPay',
            LiqpayPaymentMethod::CODE . '_subtitle' => '',
            CashPaymentMethod::CODE . '_title' => 'Готівкою',
            CashPaymentMethod::CODE . '_subtitle' => '',
            PayPartsPaymentMethod::CODE . '_title' => 'Оплата частинами від Приватбанк',
            PayPartsPaymentMethod::CODE . '_subtitle' => 'Оплата частинами від privatbank до 4 платежів.',
            OtherPayment::CODE . '_title' => 'Мені потрібна інша форма оплати',
            OtherPayment::CODE . '_subtitle' => 'Ми зв’яжемось з вами і з’ясуємо усі деталі',
            LocalPaymentMethod::CODE . '_title' => 'Оплата на рахунок компанії',
            LocalPaymentMethod::CODE . '_subtitle' => 'Ми з вами зв’яжемося для уточнення деталей оплати.',
            CreditCardPaymentMethod::CODE . '_title' => 'Оплата банківською карткою при отриманні',
            CreditCardPaymentMethod::CODE . '_subtitle' => '',
            'enter_name' => "Введіть ім'я",
            'enter_email' => 'Введіть email',
            'enter_city' => 'Введіть назву міста',
            'enter_address' => 'Введіть вашу точну адресу',
            'count_title' => 'шт',
            'checkout_title' => 'Оформлення замовлення',
            'success_title' => 'Дякуємо за замовлення!',
            'success_description' => 'Невдовзі наш менеджер зв’яжеться з вами для уточнення деталей замовлення!',
            'success_order_number' => 'Замовлення №',
            'success_total_title' => 'Всього до оплати',
            'pay_part_select_title' => 'Нічого не вибрано',
            'pay_part_select_period' => 'Виберіть термін',
            'pay_part_name_period' => 'міс',
            'order_date_from' => 'від',
        ];
    }
}
