/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

const app = {
    init: function () {
        this.saveFields();
    },
    saveFields() {
        const saveBtn = document.querySelectorAll('.mchecheckoutSave');
        saveBtn.forEach((item) => {
            item.addEventListener('click', function (event) {
                const title = event.target.parentElement.nextSibling.nextSibling.firstChild.value;
                const id = event.target.parentElement.parentElement.id.split('-')[1] || 0;
                const block = document.createElement('div');
                block.innerHTML = '<div class="mcheckoutNoty loading"><img src="/wp-includes/images/spinner-2x.gif" /></div>';
                item.after(block);

                wp.ajax.post('saveTranslateMCheckout', {
                    data: {
                        title: title,
                        id: id,
                    }
                }).then(() => {
                    block.innerHTML = '<div class="mcheckoutNoty">Saved!</div>';

                    setTimeout(() => {
                        block.remove();
                    }, 2000)
                })
            });
        }, false);
    }
}

ready(function () {
    app.init();
});

function ready(fn) {
    if (document.readyState !== 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

