<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

namespace MCheckout\Woocommerce\Payment\Liqpay;

use InvalidArgumentException;
use MCheckout\Admin\Settings;
use MCheckout\Service\Json;
use RuntimeException;
use WC_Payment_Gateway;
use MCheckout\Service\Logger;

class LiqpayPaymentMethod extends WC_Payment_Gateway
{
    public const CODE = 'liqpay';

    public function __construct()
    {
        $this->id = 'liqpay';
        $this->icon = apply_filters('woocommerce_spg_icon', '');
        $this->has_fields = false;
        $this->method_title = __('liqpay Payment Gateway', 'liqpay');
        $this->order_button_text = apply_filters('woocommerce_spg_order_button_text', __('Оплатити', 'liqpay'));
        $this->init_form_fields();
        $this->init_settings();
        $this->title = $this->get_option('title');

        add_action('woocommerce_receipt_liqpay', [$this, 'receipt_page']);
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        add_action('woocommerce_api_wc_liqpay', [$this, 'check_ipn_response']);
    }

    public static function execute(int $orderId): array
    {
        $response = [
            'payment' => '',
            'success' => true,
            'success_url' => '',
            'error' => '',
        ];

        try {
            $response['payment'] = LiqpayPaymentMethod::generateForm($orderId);
        } catch (InvalidArgumentException $exception) {
            Logger::execute(__CLASS__ . __METHOD__ . ': ' . $exception->getMessage());
            $response['error'] = $exception->getMessage();
            $response['success'] = false;
        }

        return $response;
    }

    public function admin_options()
    {
        ?>
        <h3><?php _e('liqpay Payment Gateway', 'liqpay'); ?></h3>
        <p><?php _e('Extra payment gateway with selection for shipping methods', 'liqpay'); ?></p>
        <table class="form-table">
            <?php $this->generate_settings_html(); ?>
        </table>
        <?php
    }

    /**
     * Callback action
     */
    public function check_ipn_response(): void
    {
        global $woocommerce;

        $data = $_POST['data'] ?? '';
        $parsed_data = json_decode(base64_decode($data));
        Logger::execute(Json::encode($parsed_data), 'callback');

        if (isset($parsed_data->status)) {
            $order_id = $parsed_data->order_id;
            $status = $parsed_data->status;
            $order = wc_get_order($order_id);
            $allowStatuses = ['success', 'wait_accept', 'sandbox'];

            if (in_array($status, $allowStatuses, true)) {
                $order->update_status('completed', __('Заказ оплачен (оплата получена)', 'woocommerce'));
                $order->add_order_note(__('Клиент оплатил свой заказ', 'woocommerce'));
            } else {
                $order->update_status('failed', __('Оплата не была получена', 'woocommerce'));
                wp_redirect($order->get_cancel_order_url());
                exit;
            }
        } else {
            throw new RuntimeException('IPN Request Failure');
        }
    }

    /**
     * @return void
     */
    public function init_form_fields(): void
    {
        $this->form_fields = [
            'enabled' => [
                'title' => __('Enable/Disable', 'liqpay'),
                'type' => 'checkbox',
                'label' => __('Enable liqpay Payment Gateway', 'liqpay'),
                'default' => 'yes'
            ],
            'title' => [
                'title' => __('Title', 'liqpay'),
                'type' => 'text',
                'description' => __('Payment method title which the customer will see during checkout', 'liqpay'),
                'default' => $this->method_title,
                'desc_tip' => true,
            ],
            'sub_title' => [
                'title' => __('Sub title'),
                'type' => 'text',
                'description' => '',
                'default' => ''
            ],
            'custom_title_key' => [
                'title' => __('Translate title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_title'
            ],
            'custom_sub_title_key' => [
                'title' => __('Translate Sub title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_subtitle',
            ],
        ];
    }

    /**
     * @param $order
     *
     * @return void
     */
    public function receipt_page($order): void
    {
        echo '<p>' . __('Спасибі за ваш заказ, будь ласка, натисніть кнопку нижче, щоб заплатити.', 'woocommerce') . '</p>';
        echo $this->generate_form($order);
    }

    /**
     * Old form
     *
     * @param $order_id
     *
     * @return string
     */
    public function generate_form($order_id): string
    {
        $liqpay_public = get_option(Settings::LIQPAY_PUBLIC_FIELD);
        $liqpay_private = get_option(Settings::LIQPAY_PRIVATE_FIELD);
        $order = wc_get_order($order_id);
        $payment = new LiqPay($liqpay_public, $liqpay_private);
        $result_url = add_query_arg('wc-api', 'wc_liqpay', home_url('/'));
        $redirect_page_url = $order->get_checkout_order_received_url();

        $formData = [
            'action' => 'pay',
            'result_url' => $redirect_page_url,
            'server_url' => esc_attr($result_url),
            'amount' => $order->get_total(),
            'currency' => 'UAH',
            'description' => 'Оплата за заказ - ' . $order_id,
            'order_id' => $order_id,
            'version' => '3',
            'sandbox' => Settings::isLiqpaySandbox() ? 1 : 0,
        ];

        return $payment->cnb_form($formData);
    }

    /**
     * @param int $order_id
     *
     * @return array
     */
    public function process_payment($order_id): array
    {
        $order = wc_get_order($order_id);
        return [
            'result' => 'success',
            'redirect' => add_query_arg(
                'order',
                $order->get_id(),
                add_query_arg('key', $order->get_order_key(), get_permalink(wc_get_page_id('pay')))),
        ];
    }

    /**
     * @param $order_id
     *
     * @return string
     */
    public static function generateForm($order_id): string
    {
        $order = wc_get_order($order_id);
        $publicKey = get_option(Settings::LIQPAY_PUBLIC_FIELD);
        $privateKey = get_option(Settings::LIQPAY_PRIVATE_FIELD);
        $payment = new Liqpay($publicKey, $privateKey);
        $result_url = add_query_arg('wc-api', 'wc_liqpay', home_url('/'));
        $redirect_page_url = $order->get_checkout_order_received_url();

        return $payment->cnb_form([
            'action' => 'pay',
            'result_url' => $redirect_page_url,
            'server_url' => $result_url,
            'amount' => $order->get_total(),
            'currency' => 'UAH',
            'description' => __('Оплата за замовлення') . ' - ' . $order_id,
            'order_id' => $order_id,
            'version' => '3',
            'sandbox' => Settings::isLiqpaySandbox() ? 1 : 0,
        ]);
    }
}
