<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Payment;

use MCheckout\Woocommerce\Payment\Cash\CashPaymentMethod;
use MCheckout\Woocommerce\Payment\CreditCard\CreditCardPaymentMethod;
use MCheckout\Woocommerce\Payment\Liqpay\LiqpayPaymentMethod;
use MCheckout\Woocommerce\Payment\LocalPayment\LocalPaymentMethod;
use MCheckout\Woocommerce\Payment\OtherPayment\OtherPayment;
use MCheckout\Woocommerce\Payment\PrivatPayParts\PayPartsPaymentMethod;

class PaymentMethods
{
    public function __construct()
    {
        add_filter('woocommerce_payment_gateways', [__CLASS__, 'addPaymentMethods']);
    }

    public static function addPaymentMethods($methods)
    {
        $methods[LiqpayPaymentMethod::CODE] = LiqpayPaymentMethod::class;
        $methods[CashPaymentMethod::CODE] = CashPaymentMethod::class;
        $methods[PayPartsPaymentMethod::CODE] = PayPartsPaymentMethod::class;
        $methods[OtherPayment::CODE] = OtherPayment::class;
        $methods[LocalPaymentMethod::CODE] = LocalPaymentMethod::class;
        $methods[CreditCardPaymentMethod::CODE] = CreditCardPaymentMethod::class;

        return $methods;
    }
}
