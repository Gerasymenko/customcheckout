<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

namespace MCheckout\Woocommerce\Payment\PrivatPayParts;

use InvalidArgumentException;
use MCheckout\Admin\Settings;
use MCheckout\Service\Json;
use MCheckout\Service\Logger;
use WC_Payment_Gateway;

class PayPartsPaymentMethod extends WC_Payment_Gateway
{
    public const CODE = 'pay_parts';
    private const TITLE = 'Privat pay parts';

    public function __construct()
    {
        $this->id = self::CODE;
        $this->icon = apply_filters('woocommerce_spg_icon', '');
        $this->has_fields = false;
        $this->method_title = __(self::TITLE, 'woocommerce');

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables.
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');

        // BACS account fields shown on the thanks page and in emails.
        $this->account_details = get_option(
            'woocommerce_bacs_accounts',
            array(
                array(
                    'sort_code' => $this->get_option('sort_code'),
                ),
            )
        );

        // Actions.
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        add_action('woocommerce_api_wc_pay_parts', [$this, 'check_ipn_response']);
    }


    /**
     * Callback action
     */
    public function check_ipn_response(): void
    {
        global $woocommerce;

        $data = $_POST['data'] ?? '';
        $parsed_data = json_decode(base64_decode($data));
        Logger::execute(Json::encode($parsed_data), 'callback');

        if ($data && isset($parsed_data->paymentState)) {
            $order_id = $parsed_data->orderId;
            $status = strtolower($parsed_data->paymentState);
            $order = wc_get_order($order_id);
            $allowStatuses = ['success', 'wait_accept', 'sandbox'];

            if (in_array($status, $allowStatuses, true)) {
                $order->update_status('completed', __('Заказ оплачен (оплата получена)', 'woocommerce'));
                $order->add_order_note(__('Клиент оплатил свой заказ', 'woocommerce'));
            } else {
                $order->update_status('failed', __('Оплата не была получена', 'woocommerce'));
                wp_redirect($order->get_cancel_order_url());
                exit;
            }
        } else {
            Logger::execute(Json::encode($_POST), 'callback POST IPN Request Failure');
        }
    }

    /**
     * @param int $orderId
     * @param int $partsCount
     *
     * @return array
     */
    public static function execute(int $orderId, int $partsCount = 1): array
    {
        $response = [
            'success' => true,
            'success_url' => '',
            'payment' => false,
            'error' => '',
        ];
        try {
            $response['success_url'] = self::generateUrl($orderId, $partsCount);
        } catch (InvalidArgumentException $exception) {
            Logger::execute(__CLASS__ . __METHOD__ . ': ' . $exception->getMessage());
            $response['error'] = $exception->getMessage();
            $response['success'] = false;
        }

        return $response;
    }

    /**
     * @return void
     */
    public function init_form_fields(): void
    {

        $this->form_fields = [
            'enabled' => [
                'title' => __('Enable/Disable', 'woocommerce'),
                'type' => 'checkbox',
                'label' => '',
                'default' => 'no',
            ],
            'title' => [
                'title' => __('Title', 'woocommerce'),
                'type' => 'text',
                'description' => '',
                'default' => $this->method_title,
                'desc_tip' => true,
            ],
            'sub_title' => [
                'title' => __('Sub title'),
                'type' => 'text',
                'description' => '',
                'default' => ''
            ],
            'custom_title_key' => [
                'title' => __('Translate title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_title'
            ],
            'custom_sub_title_key' => [
                'title' => __('Translate Sub title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_subtitle',
            ],
        ];
    }

    /***
     * @param int $order_id Order ID.
     *
     * @return array
     */
    public function process_payment($order_id): array
    {
        $order = wc_get_order($order_id);

        return [
            'result' => 'success',
            'redirect' => add_query_arg(
                'order',
                $order->get_id(),
                add_query_arg('key', $order->get_order_key(), get_permalink(wc_get_page_id('pay')))),
        ];
    }

    /**
     * @param $orderId
     * @param int $partsCount
     *
     * @return string
     */
    public static function generateUrl($orderId, int $partsCount = 1): string
    {
        $order = wc_get_order($orderId);
        $storeId = get_option(Settings::PAY_PARTS_PRIVAT_FIELD['STORE_ID']);
        $password = get_option(Settings::PAY_PARTS_PRIVAT_FIELD['PASSWORD']);
        $sendBoxMode = get_option(Settings::PAY_PARTS_PRIVAT_FIELD['SEND_BOX_MODE'])['value'] ?? 'pay';
        $recipientId = get_option(Settings::PAY_PARTS_PRIVAT_FIELD['RECIPIENT_ID']);
        $prefix = get_option(Settings::PAY_PARTS_PRIVAT_FIELD['PREFIX']);
        $merchantType = get_option(Settings::PAY_PARTS_PRIVAT_FIELD['MERCHANT_TYPE'])['value'] ?? 'PP';
        $isDebug = Settings::isPayPartSandboxMode();
        $payParts = new PayParts($storeId, $password);
        $resultUrl = add_query_arg('wc-api', 'wc_pay_parts', home_url('/'));
        $redirectPageUrl = $order->get_checkout_order_received_url();
        $products = [];

        foreach ($order->get_items() as $item) {
            $products[] = [
                "name" => $item->get_name(),
                "count" => $item->get_quantity(),
                "price" => $item->get_total()
            ];
        }

        $options = [
            'ResponseUrl' => $resultUrl,
            'RedirectUrl' => $redirectPageUrl,
            'PartsCount' => $partsCount,
            'Prefix' => $prefix,
            'OrderID' => $orderId,
            'MerchantType' => $merchantType,
            'Currency' => '980',
            'ProductsList' => $products,
            'recipientId' => $recipientId,
        ];

        $payParts->setOptions($options);
        $send = $payParts->create($sendBoxMode);

        if ($isDebug) {
            Logger::execute(Json::encode($payParts->getLOG()), 'pay_parts debug');
        }

        return $payParts::REDIRECT_URL . $send['token'];
    }
}
