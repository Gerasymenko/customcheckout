<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

namespace MCheckout\Woocommerce\Payment\CreditCard;

use WC_Payment_Gateway;

class CreditCardPaymentMethod extends WC_Payment_Gateway
{
    public const CODE = 'credit_card';

    public function __construct()
    {
        $this->id = self::CODE;
        $this->icon = apply_filters('woocommerce_bacs_icon', '');
        $this->has_fields = false;
        $this->method_title = __('Credit card', 'woocommerce');

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables.
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');

        // BACS account fields shown on the thanks page and in emails.
        $this->account_details = get_option(
            'woocommerce_bacs_accounts',
            array(
                array(
                    'sort_code' => $this->get_option('sort_code'),
                ),
            )
        );

        // Actions.
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
    }

    /**
     * @return void
     */
    public function init_form_fields(): void
    {
        $this->form_fields = [
            'enabled' => [
                'title' => __('Enable/Disable', 'woocommerce'),
                'type' => 'checkbox',
                'label' => '',
                'default' => 'no',
            ],
            'title' => [
                'title' => __('Title', 'woocommerce'),
                'type' => 'text',
                'description' => '',
                'default' => $this->method_title,
                'desc_tip' => true,
            ],
            'sub_title' => [
                'title' => __('Sub title'),
                'type' => 'text',
                'description' => '',
                'default' => ''
            ],
            'custom_title_key' => [
                'title' => __('Translate title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_title'
            ],
            'custom_sub_title_key' => [
                'title' => __('Translate Sub title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_subtitle',
            ],
        ];

    }

    /**
     * Process the payment and return the result.
     *
     * @param int $order_id Order ID.
     *
     * @return array
     */
    public function process_payment($order_id): array
    {
        $order = wc_get_order($order_id);

        if ($order->get_total() > 0) {
            // Mark as on-hold (we're awaiting the payment).
            $order->update_status(apply_filters('woocommerce_bacs_process_payment_order_status', 'on-hold', $order),
                __('Awaiting CASH payment', 'woocommerce')
            );
        } else {
            $order->payment_complete();
        }

        return [
            'result' => 'success',
            'redirect' => $this->get_return_url($order),
        ];
    }
}
