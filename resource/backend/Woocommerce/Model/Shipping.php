<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Model;

class Shipping
{

    /**
     * @return array
     */
    public static function getShippingMethods(): array
    {
        $methods = [];
        $isSession = isset(WC()->session) && isset(WC()->session->get("shipping_for_package_0")['rates']);

        if ($isSession) {
            $methods_arr = WC()->session->get("shipping_for_package_0")['rates'];

            if ($methods_arr) {
                foreach ($methods_arr as $rate_key => $rate) {
                    $methodData = self::getShippingMethodData($rate->method_id . ':' . $rate->instance_id) ?: [];
                    $methods[] = [
                        'method_id' => $rate->method_id,
                        'instance_id' => $rate->instance_id,
                        'rate_id' => $rate_key,
                        'label' => $methodData['custom_title_key'] ?? '',
                        'sub_title' => $methodData['custom_sub_title_key'] ?? '',
                        'static_label' => $rate->label,
                        'cost' => $rate->cost,
                        'price' => $rate->cost,
                    ];
                }
            }
        }

        return $methods;
    }

    /**
     * @param $rateId
     *
     * @return mixed
     */
    public static function getShippingMethodData($rateId)
    {
        if (!empty($rateId)) {
            $method_key_id = str_replace(':', '_', $rateId);
            $option_name = 'woocommerce_' . $method_key_id . '_' . 'settings';

            return get_option($option_name, true);
        }

        return null;
    }
}
