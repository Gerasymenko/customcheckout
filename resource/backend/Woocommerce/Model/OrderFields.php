<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Model;

class OrderFields
{
    public const TELEGRAM_MESSAGE_FIELD = 'is_telegram_send';
    public const NOVAPOST_DELIVERY_FIELD = 'novapost_delivery';
    public const UKR_POST_DELIVERY_FIELD = 'ukr_post_delivery';
    public const DELIVERY_TYPE = 'delivery_type';
    public const SMS_SEND = 'sms_send';

    public function __construct()
    {
        add_action('woocommerce_admin_order_data_after_billing_address', [$this, 'telegramField'], 10, 1);
        add_action('woocommerce_admin_order_data_after_billing_address', [$this, 'novaPostField'], 10, 2);
        add_action('woocommerce_admin_order_data_after_billing_address', [$this, 'ukrPostField'], 10, 3);
        add_action('woocommerce_admin_order_data_after_billing_address', [$this, 'deliveryTypeField'], 10, 4);
    }

    /**
     * @param $order
     *
     * @return void
     */
    public function telegramField($order): void
    {
        $orderId = method_exists($order, 'get_id') ? $order->get_id() : $order->id;
        $field = get_post_meta($orderId, self::TELEGRAM_MESSAGE_FIELD, true);

        if ($field) {
            echo '<p><strong>' . __('Is telegram notified') . ':</strong> ' . $field . '</p>';
        }
    }

    public function novaPostField($order): void
    {
        $orderId = method_exists($order, 'get_id') ? $order->get_id() : $order->id;
        $field = get_post_meta($orderId, self::NOVAPOST_DELIVERY_FIELD, true);

        if ($field) {
            echo '<p><strong>' . __('Novapost delivery address') . ':</strong> ' . $field . '</p>';
        }
    }

    public function ukrPostField($order): void
    {
        $orderId = method_exists($order, 'get_id') ? $order->get_id() : $order->id;
        $field = get_post_meta($orderId, self::UKR_POST_DELIVERY_FIELD, true);

        if ($field) {
            echo '<p><strong>' . __('Ukrpost delivery address') . ':</strong> ' . $field . '</p>';
        }
    }

    public function deliveryTypeField($order): void
    {
        $orderId = method_exists($order, 'get_id') ? $order->get_id() : $order->id;
        $field = get_post_meta($orderId, self::DELIVERY_TYPE, true);

        if ($field) {
            echo '<p><strong>' . __('Delivery type') . ':</strong> ' . $field . '</p>';
        }
    }
}
