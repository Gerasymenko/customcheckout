<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Model;

use WC_Product_Variation;

class Cart
{
    /**
     * @return array
     */
    public static function cartResponse(): array
    {
        return [
            'cart' => self::getCart(),
            'total' => self::cartTotal(),
            'cart_count' => self::cartTotalCount(),
            'delivery' => Shipping::getShippingMethods(),
        ];
    }

    /**
     * @return array
     */
    public static function getCart(): array
    {
        $cart = WC()->cart;
        $items = isset($cart) ? WC()->cart->get_cart() : [];
        $result = [];

        if ($items) {
            foreach ($items as $key => $item) {
                $result[] = self::cartItemCard($item, $key);
            }
        }

        return $result;
    }

    /**
     * @return int
     */
    public static function cartTotal(): int
    {
        return WC()->cart ? (int)WC()->cart->cart_contents_total : 0;
    }

    /**
     * @return int
     */
    public static function cartTotalCount(): int
    {
        $cart = WC()->cart;
        $items = isset($cart) ? WC()->cart->get_cart() : [];
        $total = [];

        if ($items) {
            foreach ($items as $item) {
                $quantity = (int)$item['quantity'];
                $total[] = $quantity ?: 0;
            }
        }

        return array_sum($total);
    }

    /**
     * @return bool
     */
    public static function isCartOutOfStock(): bool
    {
        $cart = WC()->cart;
        $items = isset($cart) ? WC()->cart->get_cart() : [];

        if ($items) {
            foreach ($items as $item) {
                $product = wc_get_product($item['product_id']);
                $stock_status = $product->get_stock_status();

                if ($stock_status === 'onbackorder') {
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * @param $item
     * @param string $key
     *
     * @return array
     */
    public static function cartItemCard($item, string $key = ''): array
    {
        $product = wc_get_product($item['data']->get_id());
        $categories = wp_get_post_terms($item['product_id'], 'product_cat', ['fields' => 'names']);
        $price = $product->get_price();
        $thumb = apply_filters('woocommerce_cart_item_thumbnail', $product->get_image(), $item, $key);
        $oldPrice = get_post_meta($item['product_id'], '_sale_price', true);
        $regularPrice = get_post_meta($item['product_id'], '_regular_price', true);

        if ($product->get_type() === 'variation') {
            $variationProduct = new WC_Product_Variation($product->get_id());
            $variationDiscountPrice = $variationProduct->get_regular_price();

            if ($variationDiscountPrice !== $price) {
                $regularPrice = $variationProduct->get_regular_price();
                $oldPrice = $variationProduct->get_regular_price();
            }
        }

        return [
            'thumb' => wp_kses_post($thumb),
            'name' => $product->get_name(),
            'category' => implode(', ', $categories),
            'price' => $price,
            'old_price' => $oldPrice,
            'regular_price' => $regularPrice,
            'quantity' => $item['quantity'],
            'currency' => get_woocommerce_currency_symbol(),
            'link' => get_the_permalink($item['product_id']),
            'id' => $item['product_id'],
            'remove' => wc_get_cart_remove_url($key),
            'cart_item_key' => $key,
            'sku' => get_post_meta($item['product_id'], '_sku', true),
            'width' => get_post_meta($item['product_id'], '_width', true),
            'length' => get_post_meta($item['product_id'], '_length', true),
            'height' => get_post_meta($item['product_id'], '_height', true),
            'weight' => get_post_meta($item['product_id'], '_weight', true),
            'attributes' => self::getCartProductAttributes($item, true),
        ];
    }

    /**
     * @param $price
     *
     * @return string
     */
    public static function formatPrice($price): string
    {
        $args = [
            'decimal_separator' => wc_get_price_decimal_separator(),
            'thousand_separator' => wc_get_price_thousand_separator(),
            'decimals' => wc_get_price_decimals(),
        ];

        return number_format((int)$price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator']);
    }

    public static function clearCart()
    {
        $cart = WC()->cart;

        if (isset($cart)) {
            $cart->empty_cart();
        }
    }

    public static function getCartProductAttributes($cart_item, $flat = false)
    {
        $item_data = [];

        // Variation values are shown only if they are not found in the title as of 3.0.
        // This is because variation titles display the attributes.
        if ($cart_item['data']->is_type('variation') && is_array($cart_item['variation'])) {
            foreach ($cart_item['variation'] as $name => $value) {
                $taxonomy = wc_attribute_taxonomy_name(str_replace('attribute_pa_', '', urldecode($name)));

                if (taxonomy_exists($taxonomy)) {
                    // If this is a term slug, get the term's nice name.
                    $term = get_term_by('slug', $value, $taxonomy);
                    if (!is_wp_error($term) && $term && $term->name) {
                        $value = $term->name;
                    }
                    $label = wc_attribute_label($taxonomy);
                } else {
                    // If this is a custom option slug, get the options name.
                    $value = apply_filters('woocommerce_variation_option_name', $value, null, $taxonomy, $cart_item['data']);
                    $label = wc_attribute_label(str_replace('attribute_', '', $name), $cart_item['data']);
                }

                // Check the nicename against the title.
                if ('' === $value || wc_is_attribute_in_product_name($value, $cart_item['data']->get_name())) {
                    continue;
                }

                $item_data[] = [
                    'key' => $label,
                    'value' => $value,
                ];
            }
        }

        // Filter item data to allow 3rd parties to add more to the array.
        $item_data = apply_filters('woocommerce_get_item_data', $item_data, $cart_item);

        // Format item data ready to display.
        foreach ($item_data as $key => $data) {
            // Set hidden to true to not display meta on cart.
            if (!empty($data['hidden'])) {
                unset($item_data[$key]);
                continue;
            }
            $item_data[$key]['key'] = !empty($data['key']) ? $data['key'] : $data['name'];
            $item_data[$key]['display'] = !empty($data['display']) ? $data['display'] : $data['value'];
        }

        // Output flat or in list format.
        if (count($item_data) > 0) {
            $result = [];

            if ($flat) {
                foreach ($item_data as $data) {
                    $result[] = [
                        'name' => esc_html($data['key']) . ':',
                        'value' => wp_kses_post($data['display']),
                    ];
                }

                return $result;
            } else {
                return $item_data;
            }
        }

        return [];
    }
}
