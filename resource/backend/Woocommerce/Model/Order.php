<?php
declare(strict_types=1);

namespace MCheckout\Woocommerce\Model;

use MCheckout\Front\UserData;
use MCheckout\Woocommerce\Action\PaymentComplete;
use MCheckout\Woocommerce\Admin\Deposit\DepositFields;
use MCheckout\Woocommerce\Payment\Liqpay\LiqpayPaymentMethod;
use MCheckout\Woocommerce\Payment\PrivatPayParts\PayPartsPaymentMethod;
use MCheckout\Woocommerce\Shipping\Novapost\NovaPostMethod;
use MCheckout\Woocommerce\Shipping\Ukrpost\UkrPostMethod;
use WC_Data_Exception;
use WC_Order_Item;
use WC_Order_Item_Product;
use WC_Order_Item_Shipping;
use WC_Product_Variation;

class Order
{
    /**
     * @param $data
     *
     * @return array
     * @throws WC_Data_Exception
     */
    public static function createOrder($data): array
    {
        $order = wc_create_order();
        $cart = WC()->cart;
        $lastName = $data['contact_info']['last_name'] ?? '';
        $thirdName = $data['contact_info']['surname'] ?? '';
        $address1 = $data['delivery_info']['office'] ?? '';
        $lastNameFull = $lastName . ' ' . $thirdName;

        if (!$address1) {
            $street = $data['delivery_info']['street'] ?? '';
            $house = $data['delivery_info']['house'] ?? '';
            $houseNumber = $data['delivery_info']['house_number'] ?? '';
            $address1 = $street . ' ' . $house . ' ' . $houseNumber;
        }

        $address = [
            'first_name' => $data['contact_info']['first_name'] ?? '',
            'last_name' => trim($lastNameFull),
            'email' => $data['contact_info']['email'] ?? '',
            'phone' => $data['contact_info']['phone'] ?? '',
            'address_1' => trim($address1),
            'city' => $data['delivery_info']['city'] ?? '',
            'state' => '',
            'postcode' => '',
            'country' => 'Україна',
        ];

        $order->set_address($address);
        $order->set_address($address, 'shipping');

        foreach ($cart->get_cart() as $cart_item_key => $cart_item) {
            $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
            $order->add_product($_product, $cart_item['quantity']);
        }

        $userId = get_current_user_id();

        if ($userId) {
            update_post_meta($order->get_id(), '_customer_user', $userId);
            UserData::updateData((int)$userId, $data);
        }

        $item = new WC_Order_Item_Shipping();
        $item->set_method_id($data['delivery']['type']);
        $item->set_method_title($data['delivery']['name']);
        $order->add_item($item);
        $order->set_payment_method($data['current_payment']);
        $order->calculate_totals();
        $order->update_status('processing', $data['comment'] ?? '');
        $pendingPayments = [LiqpayPaymentMethod::CODE, PayPartsPaymentMethod::CODE];

        if (in_array($data['current_payment'], $pendingPayments, true)) {
            $order->update_status('pending');
        }
        $order->update_meta_data(DepositFields::FIELDS['HAS_DEPOSIT'], $data['depositType'] === 'full' ? 'no' : 'yes');
        $order->set_total($data['total']);
        $order->save();
        $orderId = $order->get_id();

        self::updateOrderCustomFields($orderId, $data);

        if ($orderId) {
            PaymentComplete::newOrderNotification($orderId);
        }

        switch ($data['current_payment']) {
            case PayPartsPaymentMethod::CODE :
                $response = PayPartsPaymentMethod::execute($orderId, (int)$data['pay_part_period']);
                break;
            case LiqpayPaymentMethod::CODE :
                $response = LiqpayPaymentMethod::execute($orderId);
                break;
            default:
                $response = [
                    'success' => true,
                    'success_url' => $order->get_checkout_order_received_url(),
                    'payment' => false,
                    'error' => '',
                ];
                break;
        }

        if (!$orderId) {
            $response['success'] = false;
        }

        return $response;
    }

    /**
     * @param int $orderId
     *
     * @return string
     */
    public static function getOrderStatusById(int $orderId): string
    {
        $order = wc_get_order($orderId);

        if ($order) {
            return wc_get_order_status_name($order->get_status());
        }
        return '';
    }

    /**
     * @param int $orderId
     *
     * @return array
     */
    public static function getOrderById(int $orderId): array
    {
        if ($orderId) {
            $order = wc_get_order($orderId);

            if ($order) {
                $order_date = $order->get_date_created();
                $items = $order->get_items();
                $data = $order->get_data();
                $products = [];
                $getOrderId = $_GET["order"] ?? '';
                $timeStamp = isset($order_date) ? date_i18n('d.m.Y', $order_date->getTimestamp()) : date('d.m.Y');

                if ($items) {
                    foreach ($items as $item) {
                        $products[] = self::getProductOrder($item);
                    }
                }

                $payment = WC()->payment_gateways->payment_gateways()[$order->get_payment_method()]->settings['title'] ?? '';

                return [
                    'id' => $orderId,
                    'url' => !$getOrderId ? '?order=' . $orderId : $_SERVER['REQUEST_URI'],
                    'products' => $products,
                    'delivery' => $order->get_shipping_method(),
                    'payment' => $payment,
                    'user_info' => [
                        'email' => $order->get_billing_email(),
                        'first_name' => $order->get_billing_first_name(),
                        'last_name' => $order->get_billing_last_name(),
                        'state' => $order->get_billing_state(),
                        'city' => $order->get_billing_city(),
                        'address' => $order->get_billing_address_1(),
                        'phone' => $order->get_billing_phone(),
                        'zip' => $order->get_billing_postcode(),
                    ],
                    'total' => wc_format_decimal($order->get_total(), 2),
                    'currency' => get_woocommerce_currency_symbol($order->get_currency()),
                    'status' => wc_get_order_status_name($order->get_status()),
                    'status_key' => $order->get_status(),
                    'delivery_cost' => (int)$data['shipping_total'],
                    'order_date' => $timeStamp,
                    'isDeposit' => $order->get_meta(DepositFields::FIELDS['HAS_DEPOSIT']) === 'yes',
                ];
            }
        }

        return [];
    }

    /**
     * @param WC_Order_Item $item
     *
     * @return array
     */
    public static function getProductOrder(WC_Order_Item $item): array
    {
        $placeholder_img = esc_url(wc_placeholder_img_src('woocommerce_single'));
        $itemProductId = $item->get_product_id();
        $orderItem = new WC_Order_Item_Product($item->get_id());
        $product = $orderItem->get_product();
        $productId = $product->get_id();
        $image = has_post_thumbnail($itemProductId) ? get_the_post_thumbnail_url($itemProductId, 'thumbnail') : false;
        $categories = wp_get_post_terms($itemProductId, 'product_cat', ['fields' => 'names']);
        $attachment_ids = $product->get_gallery_image_ids();
        $oldPrice = get_post_meta($productId, '_sale_price', true);
        $regularPrice = get_post_meta($productId, '_regular_price', true);
        $price = $product->get_price();

        if (!$image && !empty($attachment_ids)) {
            $image = wp_get_attachment_image_url($attachment_ids[0], 'full');
        } elseif (!$image) {
            $image = $placeholder_img;
        }

        if ($orderItem->get_type() === 'variation') {
            $variationProduct = new WC_Product_Variation($productId);
            $variationDiscountPrice = $variationProduct->get_regular_price();

            if ($variationDiscountPrice !== $price) {
                $regularPrice = $variationProduct->get_regular_price();
                $oldPrice = $variationProduct->get_regular_price();
            }
        }

        return [
            'id' => $productId,
            'link' => get_the_permalink($productId),
            'image' => $image,
            'category' => implode(', ', $categories),
            'title' => html_entity_decode(get_the_title($productId)),
            'price' => $product->get_price(),
            'old_price' => $oldPrice,
            'regular_price' => $regularPrice,
            'sku' => $product->get_sku() ?: $productId,
            'quantity' => $item->get_quantity(),
            'total' => $item->get_total(),
            'type' => $item->get_type(),
            'attributes' => self::getProductOrderItemAttributes($orderItem),
        ];
    }

    /**
     * @param int $orderId
     * @param array $data
     *
     * @return void
     */
    public static function updateOrderCustomFields(int $orderId, array $data)
    {
        if (isset($data['delivery_info']['office']) && $data['delivery']['type'] === NovaPostMethod::CODE) {
            update_post_meta($orderId, OrderFields::NOVAPOST_DELIVERY_FIELD, $data['delivery_info']['office']);
        }

        if (isset($data['delivery_info']['office']) && $data['delivery']['type'] === UkrPostMethod::CODE) {
            update_post_meta($orderId, OrderFields::UKR_POST_DELIVERY_FIELD, $data['delivery_info']['office']);
        }

        if (isset($data['delivery_info']['delivery_type'])) {
            update_post_meta($orderId, OrderFields::DELIVERY_TYPE, $data['delivery_info']['delivery_type']);
        }

        if ($data['origin_total'] && $data['origin_total'] !== $data['total']) {
            update_post_meta($orderId, DepositFields::FIELDS['DEPOSIT_PAID'], $data['total']);
            update_post_meta($orderId, DepositFields::FIELDS['ORIGIN_TOTAL'], $data['origin_total']);
            update_post_meta($orderId, DepositFields::FIELDS['DEPOSIT_AMOUNT'], $data['total']);
        }
    }

    /**
     * @param $item
     *
     * @return array
     */
    public static function getProductOrderItemAttributes($item): array
    {
        $hidden_order_itemmeta = apply_filters(
            'woocommerce_hidden_order_itemmeta',
            [
                '_qty',
                '_tax_class',
                '_product_id',
                '_variation_id',
                '_line_subtotal',
                '_line_subtotal_tax',
                '_line_total',
                '_line_tax',
                'method_id',
                'cost',
                '_reduced_stock',
                '_restock_refunded_items',
            ]
        );

        $meta_data = $item->get_all_formatted_meta_data('');
        $result = [];

        if ($meta_data) {
            foreach ($meta_data as $meta) {
                if (in_array($meta->key, $hidden_order_itemmeta, true)) {
                    continue;
                }

                $result[] = [
                    'name' => wp_kses_post($meta->display_key) . ':',
                    'value' => wp_kses_post(wp_strip_all_tags($meta->display_value)),
                ];
            }
        }

        return $result;
    }
}
