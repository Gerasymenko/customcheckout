<?php
declare(strict_types=1);

namespace MCheckout\Woocommerce\Model;

use WC_Product;

class Product
{
    /**
     * @param $product_id
     *
     * @return int
     */
    public static function getProductParentCategory($product_id): int
    {
        $cat = get_the_terms($product_id, 'product_cat');
        $id = 0;
        $term_id = 0;

        if ($cat) {
            foreach ($cat as $item) {
                if ($item->parent === 0) {
                    $id = $item->term_id;
                } else {
                    $term_id = $item->term_id;
                    break;
                }
            }

            if (!$id) {
                $ancestors = get_ancestors($term_id, 'product_cat');
                $ancestors = array_reverse($ancestors);
                $ancestors[0] ? $top_term_id = $ancestors[0] : $top_term_id = $term_id;
                $term = get_term($top_term_id, 'product_cat');
                $id = $term->term_id;
            }
        }

        return $id;
    }

    /**
     * @param $product_id
     *
     * @return object
     */
    public static function getProductDataByID($product_id): object
    {
        $image = has_post_thumbnail($product_id) ? get_the_post_thumbnail_url($product_id, 'full') : false;
        $product = wc_get_product($product_id);
        $attachment_ids = $product->get_gallery_image_ids();

        if (!$image && !empty($attachment_ids)) {
            $image = wp_get_attachment_image_url($attachment_ids[0], 'full');
        } elseif (!$image) {
            $image = self::placeholderImage();
        }

        $price = get_post_meta($product_id, '_regular_price', true);
        $sale_price = get_post_meta($product_id, '_sale_price', true);
        $quantity = get_post_meta($product_id, '_stock', true);
        $sku = get_post_meta($product_id, '_sku', true);
        $product_description = $product->get_description('');
        $description = function_exists('wpm_translate_string') ? wpm_translate_string($product_description) : $product_description;
        $defaultPrice = self::defaultVariationPrice($product);

        return (object)[
            'id' => $product_id,
            'url' => get_the_permalink($product_id),
            'thumb' => $image,
            'status' => get_post_meta($product_id, '_stock_status', true),
            'category' => wc_get_product_category_list($product_id),
            'title' => html_entity_decode(get_the_title($product_id)),
            'price' => self::formatPrice($price),
            'sale_price' => self::formatPrice($sale_price),
            'quantity' => $quantity ?: '1',
            'sku' => $sku,
            'currency' => get_woocommerce_currency_symbol(),
            'description' => $description,
            'default_price' => $defaultPrice ?: self::formatPrice($price),
            'is_variation' => $product->is_type('variable'),
        ];
    }

    /**
     * @param $price
     *
     * @return string
     */
    public static function formatPrice($price): string
    {
        $args = [
            'decimal_separator' => wc_get_price_decimal_separator(),
            'thousand_separator' => wc_get_price_thousand_separator(),
            'decimals' => wc_get_price_decimals(),
        ];

        return number_format((int)$price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator']);
    }

    /**
     * @param $product
     *
     * @return string
     */
    public static function defaultVariationPrice($product): string
    {
        if ($product->is_type('variable')) {
            $variation_min_reg_price = $product->get_variation_regular_price('min', true);
            return number_format($variation_min_reg_price, 0);
        }

        return '';
    }

    /**
     * @return string
     */
    public static function placeholderImage(): string
    {
        return esc_url(wc_placeholder_img_src('woocommerce_single')) ?? '';
    }

    /**
     * Todo refactor to faster work.
     *
     * @param $product
     * @param int $length
     *
     * @return array
     */
    public static function getProductAttributes($product, int $length = 2): array
    {
        $productId = $product->get_type() === 'simple' ? $product->get_id() : $product->get_parent_id();
        $wcProduct = new WC_Product($productId);
        $attributes = $wcProduct->get_attributes();
        $attr_data = [];
        $i = 0;

        if ($attributes) {
            foreach ($attributes as $item) {
                if ($i > $length) {
                    continue;
                }

                $value_names = $item->get_options();

                if ($item->get_terms()) {
                    $value_names = [];
                    foreach ($item->get_terms() as $term) {
                        $value_names[] = $term->name;
                    }
                }

                $attr_data[] = [
                    'name' => wc_attribute_label($item->get_name()),
                    'value' => implode(',', $value_names),
                    'terms' => $item->get_terms(),
                    'slug' => $item->get_data()
                ];

                $i++;
            }
        }

        return $attr_data;
    }
}
