<?php
declare(strict_types=1);

namespace MCheckout\Woocommerce\Model;

class Payment
{
    /**
     * @return array
     */
    public static function getPaymentMethods(): array
    {
        $methods_arr = WC()->payment_gateways->payment_gateways();
        $result = [];

        foreach ($methods_arr as $key => $method) {
            if ($method->enabled === 'yes') {
                $methodData = self::getPaymentMethodData($key) ?: [];

                $result[] = [
                    'id' => $method->id,
                    'label' => $methodData['custom_title_key'] ?? $method->method_title,
                    'sub_title' => $methodData['custom_sub_title_key'] ?? $methodData['sub_title'] ?? '',
                    'method_title' => $method->method_title,
                ];
            }
        }

        return $result;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function getPaymentMethodData(string $key)
    {
        return WC()->payment_gateways->payment_gateways()[$key]->settings;
    }
}
