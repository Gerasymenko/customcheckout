<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Ajax;

use MCheckout\Service\Logger;
use MCheckout\Traits\AjaxTrait;
use MCheckout\Woocommerce\Model\Cart;
use MCheckout\Woocommerce\Model\Product;

class ProductAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_getProductByIdModalMCheckout(): void
    {
        $product_id = self::queryPostVars('product_id');

        $product = Product::getProductDataByID($product_id);
        wp_send_json_success($product);

    }

    /**
     * @return void
     */
    public static function ajax_nopriv_removeCustomProductsMCheckout(): void
    {
        $product_id = self::queryPostVars('product_id');

        $product_cart_id = WC()->cart->generate_cart_id($product_id);
        $cart_item_key = WC()->cart->find_product_in_cart($product_cart_id);

        WC()->cart->remove_cart_item($cart_item_key);

        wp_send_json_success(Cart::cartResponse());
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_addCustomProductsMCheckout(): void
    {
        try {
            $product_id = self::queryPostVars('product_id');
            WC()->cart->add_to_cart($product_id);
        } catch (\Exception $exception) {
            Logger::execute('addCustomProducts: ' . $exception->getMessage());
        }

        wp_send_json_success(Cart::cartResponse());
    }
}
