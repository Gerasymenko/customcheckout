<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Ajax;

use MCheckout\Service\Json;
use MCheckout\Service\Logger;
use MCheckout\Service\TelegramLogger;
use MCheckout\Traits\AjaxTrait;
use MCheckout\Woocommerce\Model\Order;
use WC_Data_Exception;

class OrderAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_createOrderMCheckout(): void
    {
        $post = self::queryPostVars('data');
        $result = [];
        $depositType = $post['depositType'] ?? 'full';

        $data = [
            'total' => $post['total'],
            'payment' => $post['current_payment'],
            'contact_info' => $post['contact_info'],
            'delivery' => $post['delivery'],
            'delivery_info' => $post['delivery_info'],
            'comment' => $post['comment'],
            'current_payment' => $post['current_payment'] ?? '',
            'current_delivery' => $post['current_delivery'] ?? '',
            'depositType' => $depositType ?: 'full',
            'origin_total' => $post['origin_total'] ?? null,
            'pay_part_period' => $post['pay_part_period'] ?? 1,
        ];

        try {
            $result = Order::createOrder($data);
            TelegramLogger::Log($data);
            Logger::execute(Json::encode($data), 'info');
        } catch (WC_Data_Exception $exception) {
            Logger::execute(__CLASS__ . __METHOD__ . ': ' . $exception->getMessage());
            wp_send_json_error($result);
        }

        wp_send_json_success($result, 200);
        wp_die();
    }
}
