<?php
declare(strict_types=1);

namespace MCheckout\Woocommerce\Ajax;

use MCheckout\Service\Logger;
use MCheckout\Traits\AjaxTrait;
use MCheckout\Woocommerce\Model\Cart;

class CartAjax
{
    use AjaxTrait;

    public function __construct()
    {
        self::declaration_ajax();
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_removeFromCartMCheckout(): void
    {
        $item_key = self::queryPostVars('item_key');

        if ($item_key) {
            WC()->cart->remove_cart_item($item_key);
            wp_send_json_success(Cart::cartResponse());
        } else {
            wp_send_json_error();
        }
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_getCartMCheckout(): void
    {
        wp_send_json_success(Cart::cartResponse());
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_addToCartMCheckout(): void
    {
        $data = $_POST['data'];

        try {
            if (isset($data['variation_id']) && (int)$data['variation_id'] > 0) {
                WC()->cart->add_to_cart($data['product_id'], $data['quantity'], $data['variation_id']);
            } else {
                WC()->cart->add_to_cart($data['product_id'], $data['quantity']);
            }
        } catch (\Exception $exception) {
            Logger::execute('addToCart: ' . $exception->getMessage());
            wp_send_json_error();
        }

        wp_send_json_success();
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_addToCartSingleMCheckout(): void
    {
        $post = $_POST;

        try {
            if (isset($post['variations']) && $post['variations'] !== 'none') {
                foreach ($post['variations'] as $item) {
                    WC()->cart->add_to_cart($post['product_id'], (int)$item['current_quantity'], $item['variation_id']);
                }
            } else {
                WC()->cart->add_to_cart($post['product_id'], (int)$post['quantity']);
            }
        } catch (\Exception $exception) {
            Logger::execute('addToCartSingle: ' . $exception->getMessage());
            wp_send_json_error($post);
        }

        wp_send_json_success($post);
    }

    /**
     * @return void
     */
    public static function ajax_nopriv_changeQuantityMCheckout(): void
    {
        $item_key = self::queryPostVars('item_key');
        $quantity = self::queryPostVars('quantity');

        if ($item_key) {
            WC()->cart->set_quantity($item_key, $quantity);
            wp_send_json_success(Cart::cartResponse());
        } else {
            wp_send_json_error();
        }
    }
}
