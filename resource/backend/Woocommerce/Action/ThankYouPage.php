<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Action;

use MCheckout\Admin\Settings;
use MCheckout\Service\TurboSMS;
use MCheckout\Woocommerce\Model\Order;
use MCheckout\Woocommerce\Model\OrderFields;

class ThankYouPage
{
    public function __construct()
    {
        add_action('woocommerce_thankyou', [$this, 'thankYouPage']);
    }

    /**
     * @param $order_id
     *
     * @return void
     */
    public function thankYouPage($order_id): void
    {
        if (!$order_id) {
            return;
        }

        $order = Order::getOrderById($order_id);
        $phone = $order['user_info']['phone'] ?? '';
        $is_sms = (int)get_post_meta($order_id, OrderFields::SMS_SEND, true);
        $sms = new TurboSMS();

        if ($phone && !$is_sms && $order['status_key'] == 'completed' || !$is_sms && $order['status_key'] == 'processing') {
            $sms->sendSMS([
                'phone' => $order['user_info']['phone'],
            ]);
            update_post_meta($order_id, OrderFields::SMS_SEND, 1, $order_id);
        }

        wp_redirect(Settings::getSuccessPage() . '?order=' . $order_id, 301);
    }
}
