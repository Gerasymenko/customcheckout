<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Action;

use MCheckout\Service\Telegram;
use MCheckout\Woocommerce\Model\Product;

class PaymentComplete
{
    public function __construct()
    {
        add_action('woocommerce_payment_complete', [__CLASS__, 'newOrderNotification', 20, 1]);
    }

    /**
     * @param $order_id
     *
     * @return void
     */
    public static function newOrderNotification($order_id): void
    {
        $order = wc_get_order($order_id);
        $billing_email = $order->get_billing_email();
        $billing_phone = $order->get_billing_phone();
        $billing_first_name = $order->get_billing_first_name();
        $billing_last_name = $order->get_billing_last_name();
        $address = $order->get_billing_address_1() . ' ' . $order->get_billing_address_2();
        $products = [];
        $cat_id = 0;

        foreach ($order->get_items() as $item) {
            $product_id = $item->get_product_id();
            $name = $item->get_name();
            $quantity = $item->get_quantity();
            $total = $item->get_total();
            $cat_id = Product::getProductParentCategory($product_id);

            $products[] = [
                'id' => $product_id,
                'name' => $name,
                'total' => $total,
                'quantity' => $quantity,
            ];
        }

        $shipping = [];

        foreach ($order->get_shipping_methods() as $shipping_method) {
            $shipping[] = $shipping_method->get_name();
        }

        $data = [
            'order_id' => $order_id,
            'date' => date_format($order->get_date_created(), 'd.m.Y H:i:s'),
            'email' => $billing_email,
            'phone' => $billing_phone,
            'name' => $billing_first_name . ' ' . $billing_last_name,
            'address' => $address,
            'total' => $order->get_total(),
            'products' => $products,
            'cat_id' => $cat_id,
            'payment' => $order->get_payment_method(),
            'shipping' => implode(', ', $shipping),
            'delivery_date' => get_post_meta($order_id, '_billing_delivery_date'),
            'delivery_time' => get_post_meta($order_id, '_billing_delivery_time'),
        ];

        Telegram::sendTelegramMessage($data);
    }
}
