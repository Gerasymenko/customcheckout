<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Action;

use MCheckout\Admin\Settings;
use MCheckout\Service\Logger;

class TemplateRedirect
{
    public function __construct()
    {
        add_action('template_redirect', [$this, 'paymentRedirect']);
    }

    /**
     * @return void
     */
    public function paymentRedirect()
    {
        if (is_wc_endpoint_url('order-received')) {
            global $wp;

            Logger::execute($wp->request, 'info');
            // Get the order ID
            $int = filter_var($wp->request, FILTER_SANITIZE_NUMBER_INT);
            $orderId = str_replace('-', '', $int);
            wp_redirect(Settings::getSuccessPage() . '?order=' . $orderId, 301);
        }
    }
}
