<?php
declare(strict_types=1);

namespace MCheckout\Woocommerce\Admin\Deposit;

class DepositFields
{
        public const FIELDS = [
            'HAS_DEPOSIT' => '_wc_deposits_order_has_deposit',
            'DEPOSIT_PAID' => '_wc_deposits_deposit_paid',
            'ORIGIN_TOTAL' => '_wc_deposits_original_total',
            'DEPOSIT_AMOUNT' => '_wc_deposits_deposit_amount',
            'SECOND_PAYMENT' => '_wc_deposits_second_payment',
            'SECOND_PAYMENT_PAID' => '_wc_deposits_second_payment_paid',
        ];
}
