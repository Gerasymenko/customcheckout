<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */
declare(strict_types=1);

namespace MCheckout\Woocommerce\Admin\Deposit;

use MCheckout\Admin\Settings;

class DepositListOrder
{
    public function __construct()
    {
        if (Settings::isDepositEnable()) {
            add_filter('manage_edit-shop_order_columns', [$this, 'addHasDepositColumn']);
            add_action('manage_shop_order_posts_custom_column', [$this, 'populateHasDepositColumn']);
        }
    }

    /**
     * @param $columns
     *
     * @return array
     */
    public function addHasDepositColumn($columns): array
    {
        $new_columns = [];

        foreach ($columns as $key => $column) {
            if ($key === 'order_total') {
                $new_columns['wc_deposits_has_deposit'] = 'Has Deposit';
            }

            $new_columns[$key] = $column;
        }

        return $new_columns;

    }

    /**
     * @param $column
     *
     * @return void
     */
    public function populateHasDepositColumn($column): void
    {
        if ('wc_deposits_has_deposit' === $column) {

            global $post;
            $order = wc_get_order($post->ID);

            if ($order) {
                $order_has_deposit = $order->get_meta(DepositFields::FIELDS['HAS_DEPOSIT'], true);

                if ($order_has_deposit === 'yes') {
                    echo '<span class="button wcdp_has_deposit">&#10004; Yes</span>';
                } else {
                    echo '<span class="button wcdp_no_deposit"> &#10006; No</span>';
                }
            }
        }
    }
}
