<?php
declare(strict_types=1);

namespace MCheckout\Woocommerce\Admin;

use MCheckout\Woocommerce\Admin\Deposit\DepositFields;
use MCheckout\Woocommerce\Admin\Deposit\DepositListOrder;
use MCheckout\Woocommerce\Admin\Deposit\DepositsAdminOrder;

class InitModules
{
    public function __construct()
    {
        new DepositListOrder();
        new DepositsAdminOrder();
    }
}
