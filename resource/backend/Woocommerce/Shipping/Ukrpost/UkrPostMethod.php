<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Shipping\Ukrpost;

use WC_Shipping_Method;

class UkrPostMethod extends WC_Shipping_Method
{
    public const CODE = 'ukrposhta_shippping';
    private const ID = 'ukr_post';
    private const TITLE = 'Укрпошта';
    private const DESCRIPTION = 'Укрпошта Укрпошта';

    /**
     * @access public
     * @return void
     */
    public function __construct($instance_id = 0)
    {
        parent::__construct($instance_id);

        $this->id = self::ID;
        $this->instance_id = absint($instance_id);
        $this->method_title = __(self::TITLE);
        $this->method_description = __(self::DESCRIPTION);

        $this->supports = [
            'shipping-zones',
            'instance-settings',
            'instance-settings-modal',
        ];

        $this->init();
    }

    /**
     * @return void
     */
    public function init(): void
    {
        $this->init_form_fields();
        $this->init_settings();

        $this->title = $this->get_option('title', self::TITLE);

        add_action('woocommerce_update_options_shipping_' . $this->id, [
            $this,
            'process_admin_options'
        ]);
    }

    /**
     * @return void
     */
    public function init_form_fields(): void
    {
        $this->instance_form_fields = [
            'title' => [
                'title' => __('Title', 'woocommerce'),
                'type' => 'text',
                'description' => __('Title to be display on site'),
                'default' => $this->method_title
            ],
            'sub_title' => [
                'title' => __('Sub title'),
                'type' => 'text',
                'description' => '',
                'default' => ''
            ],
            'custom_title_key' => [
                'title' => __('Translate title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_title'
            ],
            'custom_sub_title_key' => [
                'title' => __('Translate Sub title key'),
                'type' => 'text',
                'description' => '',
                'css' => 'pointer-events: none;',
                'default' => self::CODE . '_subtitle',
            ],
            'cost' => [
                'title' => __('Cost'),
                'type' => 'number',
                'description' => __('Cost of shipping'),
                'default' => 0
            ],
        ];
    }

    /**
     * @param array $package
     */
    public function calculate_shipping($package = []): void
    {
        $instanceSettings = $this->instance_settings;
        $this->add_rate(array(
                'id' => $this->id,
                'label' => $instanceSettings['title'] ?? $this->title,
                'cost' => $instanceSettings['cost'] ?? 0,
                'package' => $package,
                'taxes' => false,
            )
        );
    }
}
