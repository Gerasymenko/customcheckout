<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Shipping;

use MCheckout\Woocommerce\Shipping\LocalDelivery\LocalDeliveryMethod;
use MCheckout\Woocommerce\Shipping\Novapost\NovaPostMethod;
use MCheckout\Woocommerce\Shipping\OtherDelivery\OtherDeliveryMethod;
use MCheckout\Woocommerce\Shipping\ShopDelivery\ShopDeliveryMethod;
use MCheckout\Woocommerce\Shipping\Ukrpost\UkrPostMethod;

class ShippingMethods
{
    public function __construct()
    {
        add_filter('woocommerce_shipping_methods', [__CLASS__, 'addShippingMethods']);
    }

    public static function addShippingMethods($methods)
    {
        $methods[NovaPostMethod::CODE] = NovaPostMethod::class;
        $methods[UkrPostMethod::CODE] = UkrPostMethod::class;
        $methods[ShopDeliveryMethod::CODE] = ShopDeliveryMethod::class;
        $methods[OtherDeliveryMethod::CODE] = OtherDeliveryMethod::class;
        $methods[LocalDeliveryMethod::CODE] = LocalDeliveryMethod::class;

        return $methods;
    }
}
