<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Shortcode;

use MCheckout\Service\AssetsLoader;

class CartShortcode
{
    public function __construct()
    {
        add_shortcode('vue_cart', [__CLASS__, 'vueCart']);
    }

    public static function vueCart($atts): string
    {
        AssetsLoader::assetsCart();

        return '<div id="vue-cart"></div>';
    }
}
