<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Shortcode;

use MCheckout\Service\AssetsLoader;

class ModalCarShortcode
{
    public function __construct()
    {
        add_shortcode('vue_modal_cart', [__CLASS__, 'vueModalCart']);
    }

    public static function vueModalCart($atts): string
    {
        AssetsLoader::assetsModal();

        return '<div id="modal-cart"></div>';
    }
}
