<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Shortcode;

use MCheckout\Service\AssetsLoader;
use MCheckout\Woocommerce\Model\Order;

class SuccessShortcode
{
    public function __construct()
    {
        add_shortcode('vue_success', [__CLASS__, 'vueSuccess']);
    }

    public static function vueSuccess($atts): string
    {
        $orderId = $_GET['order'] ?? '0';

        if ($orderId) {
            $status = Order::getOrderStatusById((int)$orderId);

            if ($status === 'Completed' || !$status) {
                wp_safe_redirect('/');
            }
        }

        AssetsLoader::assetsSuccess();

        return '<div id="m-checkout-success"></div>';
    }
}
