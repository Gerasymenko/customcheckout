<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Woocommerce\Shortcode;

use MCheckout\Service\AssetsLoader;

class CheckoutShortcode
{
    public function __construct()
    {
        add_shortcode('vue_checkout', [__CLASS__, 'vueCheckout']);
    }

    public static function vueCheckout($atts): string
    {
        AssetsLoader::assetsCheckout();

        return '<div id="m-checkout-checkout"></div>';
    }
}
