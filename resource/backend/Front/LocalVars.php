<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Front;

use MCheckout\Admin\Settings;
use MCheckout\Model\TranslatesModel;
use MCheckout\Woocommerce\Model\Cart;
use MCheckout\Woocommerce\Model\Order;
use MCheckout\Woocommerce\Model\Payment;
use MCheckout\Woocommerce\Model\Shipping;

class LocalVars
{
    public function __construct()
    {
        add_action('init', [$this, 'init']);
    }

    /**
     * @return void
     */
    public function init(): void
    {
        add_action('wp_enqueue_scripts', [$this, 'globalLocalVars']);
    }

    /**
     * @return void
     */
    public function globalLocalVars(): void
    {
        $orderId = isset($_GET['order']) ? (int)$_GET['order'] : 0;

        wp_localize_script('jquery', 'ajaxurl', [admin_url('admin-ajax.php')]);
        wp_localize_script('jquery', 'mCheckout', [
            'isLogin' => is_user_logged_in(),
            'pluginUrl' => M_CHECKOUT_URL,
            'siteName' => get_bloginfo('name', ''),
            'siteLogo' => get_option(Settings::SETTINGS_FIELD['LOGO_URL'], ''),
            'novapostToken' => get_option(Settings::NOVAPOST_TOKEN_FIELD),
            'currency' => get_woocommerce_currency_symbol(),
            'cartProducts' => Cart::getCart(),
            'total' => Cart::cartTotal(),
            'delivery' => Shipping::getShippingMethods(),
            'payment' => Payment::getPaymentMethods(),
            'cart_count' => Cart::cartTotalCount(),
            'order' => Order::getOrderById($orderId),
            'user' => UserData::getData(),
            'translates' => TranslatesModel::getList(),
            'isDebug' => Settings::isNeedDebug(),
            'deposit' => [
               'isDepositEnable' => Settings::isDepositEnable() && Cart::isCartOutOfStock(),
                'value' => get_option(Settings::DEPOSIT_FIELD['VALUE']),
                'type' => get_option(Settings::DEPOSIT_FIELD['TYPE']),
            ]
        ]);
    }
}
