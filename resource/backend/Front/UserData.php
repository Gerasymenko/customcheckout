<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Front;

class UserData
{
    public static function getData(): array
    {
        $user_id = get_current_user_id();

        if (!$user_id) {
            return [];
        }

        return [
            'login' => get_user_meta($user_id, 'nickname', true),
            'user_id' => $user_id,
            'email' => get_user_meta($user_id, 'billing_email', true),
            'first_name' => get_user_meta($user_id, 'billing_first_name', true),
            'last_name' => get_user_meta($user_id, 'billing_last_name', true),
            'state' => get_user_meta($user_id, 'billing_state', true),
            'city' => get_user_meta($user_id, 'billing_city', true),
            'zip' => get_user_meta($user_id, 'billing_postcode', true),
            'address' => get_user_meta($user_id, 'billing_address_1', true),
            'phone' => get_user_meta($user_id, 'billing_phone', true),
            'logout_url' => str_replace('&amp;', '&', wp_logout_url('/')),
        ];
    }

    public static function updateData(int $userId, array $data)
    {
        $firstName = $data['contact_info']['first_name'] ?? '';
        $email = $data['contact_info']['email'] ?? '';
        $phone = $data['contact_info']['phone'] ?? '';

        if ($email) {
            update_user_meta($userId, 'billing_email', $email);
        }

        if ($firstName) {
            update_user_meta($userId, 'billing_first_name', $firstName);
        }

        if ($phone) {
            update_user_meta($userId, 'billing_phone', $phone);
        }
    }
}
