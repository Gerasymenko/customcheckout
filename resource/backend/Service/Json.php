<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Service;

use RuntimeException;

class Json
{
    /**
     * @param mixed $value
     *
     * @return string
     */
    public static function encode($value): string
    {
        if (is_string($value)) {
            return $value;
        }

        $json = json_encode($value, JSON_UNESCAPED_SLASHES | JSON_PRESERVE_ZERO_FRACTION);
        $error = json_last_error();

        if ($error || $json === false) {
            throw new RuntimeException(json_last_error_msg(), (string)$error);
        }
        return $json;
    }

    /**
     * @param string $json
     *
     * @return array
     */
    public static function decode(string $json): array
    {
        $value = json_decode($json, true);
        $error = json_last_error();

        if ($error) {
            throw new RuntimeException(json_last_error_msg(), (string)$error);
        }

        return $value;
    }
}
