<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Service;

use MCheckout\Admin\Settings;
use MCheckout\Model\Resource\LogResourceModel;

class Logger
{
    /**
     * @param string $message
     * @param string $action
     */
    public static function execute(string $message, string $action = 'error'): void
    {
        if (Settings::isNeedLogs()) {
            LogResourceModel::set([
                'action' => $action,
                'message' => $message,
            ]);
        }
    }
}
