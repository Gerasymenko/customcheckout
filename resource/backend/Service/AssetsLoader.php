<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Service;

use MCheckout\Admin\Settings;

class AssetsLoader
{
    private const DIST_PATH = 'resource/frontend/dist/';
    private const STATIC_KEY = 'static_version';
    private const KEY = 'MCheckout_Service';

    public static function assetsAdmin(): void
    {
        self::simpleRegister(self::KEY . '_admin', self::distUrl('admin/admin.js'), false, false, true);
        self::simpleRegister(self::KEY . '_admin', self::distUrl('admin/admin.css'), true, false);
    }

    public static function assetsCart(): void
    {
        self::simpleRegister(self::KEY . '_cart', self::distUrl('cart/cart.js'), false, false, true);
        self::simpleRegister(self::KEY . '_cart', self::distUrl('cart/cart.css'), true, false);
    }

    public static function assetsCheckout(): void
    {
        self::simpleRegister(self::KEY . '_checkout', self::distUrl('checkout/checkout.js'), false, false, true);
        self::simpleRegister(self::KEY . '_checkout', self::distUrl('checkout/checkout.css'), true, false);
    }

    public static function assetsModal(): void
    {
        self::simpleRegister(self::KEY . '_modal', self::distUrl('modal/modal.js'), false, false, true);
        self::simpleRegister(self::KEY . '_modal', self::distUrl('modal/modal.css'), true, false);
    }

    public static function assetsSuccess(): void
    {
        self::simpleRegister(self::KEY . '_success', self::distUrl('success/success.js'), false, false, true);
        self::simpleRegister(self::KEY . '_success', self::distUrl('success/success.css'), true, false);
    }

    /**
     * @param $name
     * @param $path
     * @param bool $style
     * @param bool $register
     * @param bool $footer
     */
    public static function simpleRegister($name, $path, bool $style = false, bool $register = true, bool $footer = false): void
    {
        $staticVersion = wp_date('dY');

        $ver = Settings::isNeedCache() ? $staticVersion : microtime();

        if ($register) {
            if ($style) {
                wp_register_style($name, $path, [], $ver);
            } else {
                wp_register_script($name, $path, array('jquery'), $ver, true);
            }

        } else {
            $ver = Settings::isNeedCache() ? $staticVersion : microtime();

            if ($style) {
                wp_enqueue_style($name, $path, [], $ver);
            } else {
                wp_enqueue_script($name, $path, ['jquery'], $ver, $footer);
            }
        }
    }

    /**
     * @param $file
     *
     * @return string
     */
    public static function distUrl($file): string
    {
        return M_CHECKOUT_URL . self::DIST_PATH . $file;
    }
}
