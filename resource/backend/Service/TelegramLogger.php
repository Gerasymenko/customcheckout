<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */
declare(strict_types=1);

namespace MCheckout\Service;

use MCheckout\Admin\Settings;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;

class TelegramLogger
{
    /**
     * @param $data
     *
     * @return void
     */
    public static function Log($data): void
    {
        $chatId = Telegram::getChatId();
        $isDebug = Settings::isTelegramDebug();

        if ($isDebug && $chatId) {
            $token = get_option(Settings::TELEGRAM_FIELD['TOKEN']);

            try {
                $api = new Api($token);
                $api->sendMessage(
                    [
                        'chat_id' => (int)$chatId,
                        'text' => json_encode($data),
                    ]
                );
            } catch (TelegramSDKException $exception) {
                Logger::execute(__CLASS__ . __METHOD__ . $exception->getMessage() . PHP_EOL);
            }
        }
    }
}
