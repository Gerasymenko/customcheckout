<?php
/*
 * Copyright (c) 2022.
 *  Created by metasync.site.
 *  Developer: gerasymenkoph@gmail.com
 *  Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Service;

use Exception;
use MCheckout\Admin\Settings;
use SoapClient;

class TurboSMS
{
    /**
     * @param array $data
     *
     * @return void
     */
    public function sendSMS(array $data): void
    {
        $this->TurboInit($data);
    }

    /**
     * @param array $data
     *
     * @return void
     */
    public function TurboInit(array $data): void
    {
        if (get_option(Settings::SMS_FIELD['SENDER'])) {
            header('Content-type: text/html; charset=utf-8');
            echo '';
            try {
                $client = new SoapClient('http://turbosms.in.ua/api/wsdl.html'); //todo admin
                $auth = [
                    'login' => get_option(Settings::SMS_FIELD['LOGIN']),
                    'password' => get_option(Settings::SMS_FIELD['PASSWORD'])
                ];
                $client->Auth($auth);
                $client->GetCreditBalance();
                $sms = [
                    'sender' => get_option(Settings::SMS_FIELD['SENDER']),
                    'destination' => '+38' . $data['phone'],
                    'text' => get_option(Settings::SMS_FIELD['TEXT']),
                ];

                $client->SendSMS($sms);
                $client->GetMessageStatus($sms);
            } catch (Exception $e) {
                Logger::execute(__CLASS__ . __METHOD__ . $e->getMessage() . PHP_EOL);
            }

            echo '';
        }
    }
}
