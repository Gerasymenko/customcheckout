<?php
/*
 * Copyright (c) 2022.
 * Created by metasync.site.
 * Developer: gerasymenkoph@gmail.com
 * Link: https://t.me/gerasart
 */

declare(strict_types=1);

namespace MCheckout\Service;

use MCheckout\Admin\Settings;
use MCheckout\Woocommerce\Model\OrderFields;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Api;

class Telegram
{
    /**
     * @param array $data
     *
     * @return void
     */
    public static function sendTelegramMessage(array $data): void
    {
        $isTelegram = get_post_meta($data['order_id'], OrderFields::TELEGRAM_MESSAGE_FIELD, true);
        $chatId = self::getChatId();

        if (!$isTelegram && $chatId) {
            $text = self::messageText($data);
            $token = get_option(Settings::TELEGRAM_FIELD['TOKEN']);

            try {
                $api = new Api($token);
                $api->sendMessage(
                    [
                        'chat_id' => $chatId,
                        'text' => $text,
                    ]
                );
            } catch (TelegramSDKException $exception) {
                Logger::execute(__CLASS__ . __METHOD__ . $exception->getMessage() . PHP_EOL);
            }

            update_post_meta($data['order_id'], OrderFields::TELEGRAM_MESSAGE_FIELD, 'yes');
        }
    }

    /**
     * @return false|mixed|string
     */
    public static function getChatId()
    {
        $chatIds = get_option(Settings::TELEGRAM_FIELD['IDS']);

        if (!$chatIds) {
            return false;
        }

        $chatIdsToArray = explode(',', $chatIds);

        return $chatIdsToArray[0] ?? false;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private static function messageText(array $data): string
    {
        return "🔔Нове замовлення (№{$data['order_id']}) - {$data['date']}🔔 \n\n";
    }
}
