<?php
/**
 *  Template name: MCheckout Success
 */

use MCheckout\Woocommerce\Model\Cart;

if (!isset($_GET['order']) || $_GET['order'] === '0') {
    wp_redirect('/');
}

if (function_exists('gtm4wp_woocommerce_thankyou')) {
    gtm4wp_woocommerce_thankyou($_GET['order']);
}
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="primary" class="container">
    <main id="main" class="site-main" role="main">
        <?= do_shortcode('[vue_success]'); ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php Cart::clearCart(); ?>
<?php wp_footer(); ?>
</body>
</html>
