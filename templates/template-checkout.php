<?php
/**
 *  Template name: MCheckout Checkout
 */
 ?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="primary" class="container">
		<main id="main" class="site-main" role="main">
            <?= do_shortcode('[vue_checkout]'); ?>
        </main><!-- #main -->
	</div><!-- #primary -->

    <?php wp_footer(); ?>
</body>
</html>
