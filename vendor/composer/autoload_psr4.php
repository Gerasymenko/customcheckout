<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'voku\\' => array($vendorDir . '/voku/portable-ascii/src/voku'),
    'Telegram\\Bot\\' => array($vendorDir . '/irazasyed/telegram-bot-sdk/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Contracts\\Translation\\' => array($vendorDir . '/symfony/translation-contracts'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Nadar\\PhpComposerReader\\' => array($vendorDir . '/nadar/php-composer-reader/src'),
    'MCheckout\\Woocommerce\\Shortcode\\' => array($baseDir . '/resource/backend/Woocommerce/Shortcode'),
    'MCheckout\\Woocommerce\\Shipping\\' => array($baseDir . '/resource/backend/Woocommerce/Shipping'),
    'MCheckout\\Woocommerce\\Payment\\' => array($baseDir . '/resource/backend/Woocommerce/Payment'),
    'MCheckout\\Woocommerce\\Model\\' => array($baseDir . '/resource/backend/Woocommerce/Model'),
    'MCheckout\\Woocommerce\\Ajax\\' => array($baseDir . '/resource/backend/Woocommerce/Ajax'),
    'MCheckout\\Woocommerce\\Admin\\' => array($baseDir . '/resource/backend/Woocommerce/Admin'),
    'MCheckout\\Woocommerce\\Action\\' => array($baseDir . '/resource/backend/Woocommerce/Action'),
    'MCheckout\\Traits\\' => array($baseDir . '/resource/backend/Traits'),
    'MCheckout\\Service\\' => array($baseDir . '/resource/backend/Service'),
    'MCheckout\\Model\\Resource\\' => array($baseDir . '/resource/backend/Model/Resource'),
    'MCheckout\\Model\\' => array($baseDir . '/resource/backend/Model'),
    'MCheckout\\Front\\' => array($baseDir . '/resource/backend/Front'),
    'MCheckout\\Admin\\Ajax\\' => array($baseDir . '/resource/backend/Admin/Ajax'),
    'MCheckout\\Admin\\' => array($baseDir . '/resource/backend/Admin'),
    'LibDNS\\' => array($vendorDir . '/daverandom/libdns/src'),
    'League\\Event\\' => array($vendorDir . '/league/event/src'),
    'Kozz\\' => array($vendorDir . '/kozz/emoji-regex/src'),
    'Kelunik\\Certificate\\' => array($vendorDir . '/kelunik/certificate/lib'),
    'Illuminate\\Support\\' => array($vendorDir . '/illuminate/collections', $vendorDir . '/illuminate/macroable', $vendorDir . '/illuminate/support'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
    'Http\\Factory\\Guzzle\\' => array($vendorDir . '/http-interop/http-factory-guzzle/src'),
    'HaydenPierce\\ClassFinder\\UnitTest\\' => array($vendorDir . '/haydenpierce/class-finder/test/unit'),
    'HaydenPierce\\ClassFinder\\' => array($vendorDir . '/haydenpierce/class-finder/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Doctrine\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Inflector'),
    'Composer\\Semver\\' => array($vendorDir . '/composer/semver/src'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'Amp\\WindowsRegistry\\' => array($vendorDir . '/amphp/windows-registry/lib'),
    'Amp\\Uri\\' => array($vendorDir . '/amphp/uri/src'),
    'Amp\\Sync\\' => array($vendorDir . '/amphp/sync/src'),
    'Amp\\Socket\\' => array($vendorDir . '/amphp/socket/src'),
    'Amp\\Serialization\\' => array($vendorDir . '/amphp/serialization/src'),
    'Amp\\Process\\' => array($vendorDir . '/amphp/process/lib'),
    'Amp\\Parser\\' => array($vendorDir . '/amphp/parser/lib'),
    'Amp\\Parallel\\' => array($vendorDir . '/amphp/parallel/lib'),
    'Amp\\File\\' => array($vendorDir . '/amphp/file/lib'),
    'Amp\\Dns\\' => array($vendorDir . '/amphp/dns/lib'),
    'Amp\\Cache\\' => array($vendorDir . '/amphp/cache/lib'),
    'Amp\\ByteStream\\' => array($vendorDir . '/amphp/byte-stream/lib'),
    'Amp\\Artax\\' => array($vendorDir . '/amphp/artax/lib'),
    'Amp\\' => array($vendorDir . '/amphp/amp/lib'),
);
